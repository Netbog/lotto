<?php
// Template Name: Big Lotteries
get_header('home'); ?>
<style>
.pagination, .woocommerce-pagination {
   float: none; 
   margin-top:  0px; 
    text-align: center;
 }
</style>
 
<script src="<?php echo get_template_directory_uri();?>/js/jquery.validate.js" type="text/javascript"></script>
<script>jQuery(document).ready(function($){
$("#help_form").validate({
debug: false,
rules: {
name: "required",
email: {
required: true,	
email: true 
},
phone:"required", 
    },
messages: {
name: "Please enter name.",
email: "Pleasea enter email.",
phone:"Please enter phone number",
   },
submitHandler: function(form) {
jQuery.ajax({
type: 'POST',
url: MyAjax.ajaxurl,
data:jQuery(form).serialize(),
success: function(result){ 
alert(result);
$( '#help_form' ).each(function(){this.reset();}); 
 }
});
}
});

 

 });
</script>
<?php echo do_shortcode('[php snippet=91]');?>

<div class="container_bg" style="padding-top:20px; ">
  <div class="container">
    <div class="new_left_container">
      <?php while(have_posts()): the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; ?>
      
 <div class="newbox">  <?php      
 if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }

elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }

else { $paged = 1; }

 
$args = array (
'category_name' => 'news','paged' => $paged,
'posts_per_page' => 7,
 'order' => 'desc', 

);
$temp = $wp_query;  
$wp_query = null;
$wp_query = new WP_Query($args); 

?>
<?php 
        if ( $wp_query->have_posts() ) :
            while ( $wp_query->have_posts() ) : $wp_query->the_post();
        
        ?>
       <div class="newbox-inner"><a href="<?php echo get_permalink($post->ID);?>" >  <h2><?php  echo get_the_title($post->ID);?>  </h2></a>
            
           <?php 
     $feat=wp_get_attachment_url(get_post_thumbnail_id($post_id));
 					if($feat){	?>
   <img src="<?php echo $feat;?>" alt="<?php the_title();?>" width="150" class="alignleft"/>  
      <?php }  ?> <?php echo custom_excerpt(45);?>...
		  <p><a href="<?php echo get_permalink($post->ID);?>" class="read_more"><img src="http://thetoplotto.com/wp-content/themes/Avada/Avada/images/read_more.png"/></a> </p>
		   <div class="clear"></div>	</div>   <?php 
 	
            endwhile;
 			wp_reset_postdata();
   
	  
	  	  themefusion_pagination($pages = '', $range =4);  

	  $wp_query = NULL;
	  $wp_query = $temp_query;


        else :
          endif;
        $wp_query = $temp;
        ?></div>
    </div>
    <div class="new_right_container">
    
    
    <?php dynamic_sidebar( 'big_lotteries_widget' ); ?>
 
    </div>
    <div class="clear"></div>
    
      
      <?php wp_reset_query();  $content_post38 = get_post(38);
$content38 = $content_post38->post_content;
$content38 = apply_filters('the_content', $content38);
$content38 = str_replace(']]>', ']]&gt;', $content38);
echo $content38; wp_reset_query();
?>

  </div>
</div>
<?php get_footer('home'); ?>
