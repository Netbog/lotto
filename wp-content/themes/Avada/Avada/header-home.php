<!DOCTYPE html>
<html xmlns="http<?php echo (is_ssl())? 's' : ''; ?>://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"  />

 
	<title>
	<?php
	if ( defined('WPSEO_VERSION') ) {
		wp_title('');
	} else {
		bloginfo('name'); ?> <?php wp_title(' - ', true, 'left');
	}
	?>
	</title>
	<?php global $data; ?>

	 
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href="<?php echo get_template_directory_uri();?>/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_template_directory_uri();?>/main.css" rel="stylesheet" type="text/css" />
 <link href="<?php echo get_template_directory_uri();?>/responsive.css" rel="stylesheet" type="text/css" />
<!-- MNEU CSS AND JS -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/menu.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/fonts/fontawesome.css"/>
 
 <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.min.js"></script> 
 
  <script src="<?php echo get_template_directory_uri();?>/js/animated-menu.js" type="text/javascript"></script>
 <script src="<?php echo get_template_directory_uri();?>/js/jquery.vticker.js?v=1.15"></script>
<script>
  $(function() {
$('#example').vTicker('init', {speed:1200, 
    pause: 1500,
    showItems: 5,
    padding:6});  });
</script> 
 <?php if(is_page('comparison-sites-lottery-large-format')){?>
<script type="text/javascript">
	var doc = document.documentElement;
	doc.setAttribute('data-useragent', navigator.userAgent);
	<?php
	ob_start();
	include_once get_template_directory() . '/framework/dynamic_js.php';
	$dynamic_js = ob_get_contents();
	ob_get_clean();

	echo $dynamic_js;

	?>
	</script>
<?php } ?>
	<?php wp_head(); ?>

<body>



<div class="web">
  <div class="header_bg">
    <div class="header">
      <div class="logo"><a href="<?php bloginfo('url'); ?>">
				<img src="<?php echo get_template_directory_uri();?>/images/logo_new.png" alt="<?php bloginfo('name'); ?>" data-max-width="<?php echo $data["header_logo_max_width"]; ?>"  /></a></div>
      <div class="header_right_top">
        <ul>
          <li><a href="<?php echo site_url();?>/blog/">Blog</a> </li>
          <li> <a href="<?php echo site_url();?>/contact/">Contact</a> </li>
        </ul>
        <form role="search" method="get" class="search_form" action="<?php echo site_url();?>">
          <input type="search" class="search_field" required placeholder="Search..." value="" name="s" title="Search for:" />
          </label>
          <input type="submit" class="search_submit" value="Search" />
        </form>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="nav_bg">
    <div class="nav">
    <?php  wp_nav_menu( array( 'container' => '', 'theme_location' => 'home_new' ) ); ?>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>

<div class="mobile_header">
  <div class="header_bg">
    <div class="header">
      <div class="logo"><a href="/"><img src="<?php echo get_template_directory_uri();?>/images/mlogo.png" alt="" /></a></div>
      <div class="nav">
        <?php  wp_nav_menu( array( 'container' => '', 'theme_location' => 'home_new' ) ); ?>

      <div class="clear"></div>
    </div>
      <div class="header_right_top">
        <form role="search" method="get" class="search_form" action=" ">
          <input type="search" class="search_field" placeholder="Search..." value="" name="s" title="Search for:" />
          </label>
          <input type="submit" class="search_submit" value="Search" />
        </form>
        <div class="clear"></div>
      </div>
        <div class="clear"></div>

    </div>
    <div class="clear"></div>
  </div>
   
  <div class="clear"></div>
</div>

<div class="spacer_top">&nbsp;</div>
<?php if(!is_front_page()){?>
<?php echo do_shortcode('[php snippet=96]');?>
<?php } else{?>
<?php echo do_shortcode('[php snippet=98]');?>
<?php  } ?>
 