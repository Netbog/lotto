<?php global $data; ?>
<div class="header-v1">
	<header id="header">
		<div style="margin-top:<?php echo $data['margin_header_top']; ?>;margin-bottom:<?php echo $data['margin_header_bottom']; ?>; background-color:#000; height:154px">
			<div class="logo" style="margin-right:<?php echo $data['margin_logo_right']; ?>;margin-top:<?php echo $data['margin_logo_top']; ?>;margin-left:<?php echo $data['margin_logo_left']; ?>;margin-bottom:<?php echo $data['margin_logo_bottom']; ?>;">
				<a href="<?php bloginfo('url'); ?>">
					<img src="<?php echo $data['logo']; ?>" alt="<?php bloginfo('name'); ?>" class="normal_logo" />
					<?php if($data['logo_retina'] && $data['retina_logo_width'] && $data['retina_logo_height']): ?>
					<?php
					$pixels ="";
					if(is_numeric($data['retina_logo_width']) && is_numeric($data['retina_logo_height'])):
					$pixels ="px";
					endif; ?>
					<img src="<?php echo $data["logo_retina"]; ?>" alt="<?php bloginfo('name'); ?>" style="width:<?php echo $data["retina_logo_width"].$pixels; ?>;max-height:<?php echo $data["retina_logo_height"].$pixels; ?>; height: auto !important" class="retina_logo" />
					<?php endif; ?>
				</a>
			</div>
            <div style="float:right;color:#FFF;margin-right: 50px;"><a href="http://thetoplotto.com/login-page/" style="color:#FFF">Log In</a> | <a href="http://thetoplotto.com/registration-page/" style="color:#FFF">Register</a><?php //wp_login_form(); ?><!--&nbsp;&nbsp;&nbsp;<a href="http://thetoplotto.com/" ><img class="iclflag" src="http://thetoplotto.com/wp-content/plugins/sitepress-multilingual-cms.3.1.5/res/flags/en.png" alt="en" title="English" style="margin-top: 5px;"></a>&nbsp;&nbsp;&nbsp;<a href="http://thetoplotto.com/fr/" ><img class="iclflag" src="http://thetoplotto.com/wp-content/plugins/sitepress-multilingual-cms.3.1.5/res/flags/fr.png" alt="fr" title="Français" style="margin-top: 5px;"></a>--><?php //do_action('icl_language_selector'); ?>
            
<?php if($data['main_nav_search_icon'] && !$data['ubermenu']): ?>

	<span class="main-nav-search">
		<a id="main-nav-search-link" class="search-link" style="color:#FFF"></a>
		<div id="main-nav-search-form" class="main-nav-search-form">
			<form role="search" id="searchform" method="get" action="<?php echo home_url( '/' ); ?>">
				<input type="text" value="" name="s" id="s" />
				<input type="submit" value="&#xf002;" />
			</form>
		</div>
	</span>
    
	<?php endif; ?>
			
            </div>			
			<?php if(tf_checkIfMenuIsSetByLocation('main_navigation')): ?>
			<div class="mobile-nav-holder main-menu"></div>
			<?php endif; ?>
		</div>
	</header>
</div>
<div style="background-image: url('http://thetoplotto.com/wp-content/uploads/2014/07/bg_menu2.jpg') !important;
background-repeat: repeat-x;"><center>
<?php if($data['ubermenu']): ?>
			<nav id="nav-uber">
			<?php else: ?>
			<nav id="nav" class="nav-holder">
			<?php endif; ?>
				<?php get_template_part('framework/headers/header-main-menu'); ?>
			</nav>
</center></div>

<!--<div class="az" style="width: 100%;height: 100px;background-color: #0F0;position: absolute;z-index: 9999;"></div>-->
<div id="menudesc" style="width:100%; height:1px"></div>
<script>
/*$("a.suppa_top_links_has_arrow").click(
function()
{
	$("#menudesc").css({ 'margin-top': '280px' });	
}
);
$('body').click(function(event) { 
     $("#menudesc").css({ 'margin-top': '0px' }); 
 });*/
</script>