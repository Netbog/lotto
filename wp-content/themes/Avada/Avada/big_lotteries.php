<?php
// Template Name: Big Lotteries
get_header('home'); ?>

 
<script src="<?php echo get_template_directory_uri();?>/js/jquery.validate.js" type="text/javascript"></script>
<script>jQuery(document).ready(function($){
$("#help_form").validate({
debug: false,
rules: {
name: "required",
email: {
required: true,	
email: true 
},
phone:"required", 
    },
messages: {
name: "Please enter name.",
email: "Pleasea enter email.",
phone:"Please enter phone number",
   },
submitHandler: function(form) {
jQuery.ajax({
type: 'POST',
url: MyAjax.ajaxurl,
data:jQuery(form).serialize(),
success: function(result){ 
alert(result);
$( '#help_form' ).each(function(){this.reset();}); 
 }
});
}
});

 

 });
</script> 

<?php echo do_shortcode('[php snippet=91]');?>

<div class="container_bg" style="padding-top:20px; ">
  <div class="container">
    <div class="new_left_container">
      <?php while(have_posts()): the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; ?>
      
   
    </div>
    <div class="new_right_container">
    
    
    <?php dynamic_sidebar( 'big_lotteries_widget' ); ?>
 
    </div>
    <div class="clear"></div>
    
      
      <?php wp_reset_query();  $content_post38 = get_post(38);
$content38 = $content_post38->post_content;
$content38 = apply_filters('the_content', $content38);
$content38 = str_replace(']]>', ']]&gt;', $content38);
echo $content38; wp_reset_query();
?>

  </div>
</div>
<?php get_footer('home'); ?>
