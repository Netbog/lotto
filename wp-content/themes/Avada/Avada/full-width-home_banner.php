<?php
// Template Name: Full Width Home
get_header('home'); ?><script src="<?php echo get_template_directory_uri();?>/js/jquery.validate.js" type="text/javascript"></script>
<script>jQuery(document).ready(function($){
$("#help_form").validate({
debug: false,
rules: {
name: "required",
email: {
required: true,	
email: true 
},
phone:"required", 
    },
messages: {
name: "Please enter name.",
email: "Pleasea enter email.",
phone:"Please enter phone number",
   },
submitHandler: function(form) {
jQuery.ajax({
type: 'POST',
url: MyAjax.ajaxurl,
data:jQuery(form).serialize(),
success: function(result){ 
alert(result);
$( '#help_form' ).each(function(){this.reset();}); 
 }
});
}
});

 

 });
</script>
<?php echo do_shortcode('[php snippet=91]');?>

<div class="container_bg">
  <div class="container">
    <ul class="containerul">
      <li>
        <div class="results">
          <h2><img src="<?php echo get_template_directory_uri();?>/images/results.png" /> Lottery Results</h2>
        </div>
       <div id="example">

          <?php echo do_shortcode('[php snippet=92]');?> </div> </li>
      <li> <?php echo do_shortcode('[php snippet=90]');?> </li><div class="applyto2"></div>
      <li id="top">
        <div class="results">
          <h2><img src="<?php echo get_template_directory_uri();?>/images/star.png" /> Top 5 Lotteries</h2>
        </div>
        <?php echo do_shortcode('[php snippet=89]');?>
        <div class="clear"></div>
      </li>   <div class="applyto3"></div>
      <li>
        <div class="results">
          <h2><img src="<?php echo get_template_directory_uri();?>/images/about.png" /> About us</h2>
        </div>
        <?php echo do_shortcode('[php snippet=94]');?> </li>
        
       <div class="applyto2"></div>
      <li>
        <div class="results">
          <h2><img src="<?php echo get_template_directory_uri();?>/images/blog.png" /> Blog</h2>
        </div>
        <ol class="top" id="blog">
          <?PHP wp_reset_query();
 // WP_Query arguments
 $args = array (
 	'post_type'=> 'post','order'=> 'desc',
 	'posts_per_page'   =>3, 'category_name' => 'Blog'
 );

 // The Query
 $the_query = new WP_Query( $args );
  if ( $the_query->have_posts() ) { 
  	while ( $the_query->have_posts() ) {
 		$the_query->the_post();
  ?>
          <li>
            <p><?php //echo get_the_title($post->ID);?></p>
            <?php echo custom_excerpt(20);?>...
            <a href="<?php echo get_permalink($post->ID);?>" class="read_more">Read More>></a> </li>
          <?php 
 	}
 } 
 wp_reset_query();
 ?>
        </ol>
      </li>
     <li id="help">
        <div class="results">
          <h2><img src="<?php echo get_template_directory_uri();?>/images/help.png" /> Buy 1 Get 1 Free Tickets</h2>
        </div>
        <div class="form_layout">
          <form action="help_choose" method="post" id="help_form">
            <p>
              <label>Do you play lotto?</label>
              <select name="do_you_play" class="select">
                <option value="Yes">Yes </option>
                <option value="No">No </option>
              </select>
            </p>
            <p>
              <label>What's your favorite lottery?</label>
              <select name="fav" class="select">
                <option value="Mega Millions">Mega Millions </option>
                <option value="Powerball">Powerball </option>
                <option value="Euro millions ">Euro millions </option>
                <option value="lotto 649">lotto 649 </option>
                <option value="uk-lotto">uk-lotto</option>
              </select>
            </p>
            <p>
              <label>How do you fill your ticket: </label>
              <select name="how_fill" class="select">
                <option value="lucky numbers">lucky numbers </option>
                <option value="Quick Numbres">Quick Numbres </option>
              </select>
            </p>
            <p>
              <label>What's your lucky number?</label>
              <input name="ln1" type="text" class="input2" maxlength="1"/>
              <input name="ln2" type="text" class="input1" maxlength="1"/>
              <input name="ln3" type="text" class="input1" maxlength="1"/>
              <input name="ln4" type="text" class="input1" maxlength="1"/>
              <input name="ln5" type="text" class="input1" maxlength="1"/>
              <input name="ln6" type="text" class="input1" maxlength="1"/>
            </p>
            <p>
              <input name="name" type="text"  placeholder="Name" class="input"/>
            </p>
            <p>
              <input name="email" type="text"  placeholder="Email" class="input"/>
            </p>
            <p>
              <input name="phone" type="text"  placeholder="Phone number" class="input"/>
            </p>
            <p>
              <label class="offer">SEND ME A CUSTOM OFFER!</label>
              <span>I am over 18</span>
              <input name="send_custom_offer" type="checkbox" value="Yes" class="checkbox" />
            </p><input type="hidden" name="action" value="help_choose"/>
            <input name="submit" type="submit" value=" " class="submit" />
          </form>
        </div>
      </li> <div class="applyto2"></div> <div class="applyto3"></div>
    </ul>
    <div class="clear"></div>
  </div>
  <?php while(have_posts()): the_post(); ?>
  <?php the_content(); ?>
  <?php endwhile; ?>
  <div class="clear"></div>
</div>
<?php get_footer('home'); ?>
