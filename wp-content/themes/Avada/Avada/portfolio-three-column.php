<?php
// Template Name: Portfolio Three Column
get_header(); ?>
	<?php
	$content_css = 'width:100%';
	$sidebar_css = 'display:none';
	$content_class = '';
	if(get_post_meta($post->ID, 'pyre_portfolio_full_width', true) == 'yes') {
		$content_css = 'width:100%';
		$sidebar_css = 'display:none';
	}
	elseif(get_post_meta($post->ID, 'pyre_portfolio_sidebar_position', true) == 'left') {
		$content_css = 'float:right;';
		$sidebar_css = 'float:left;';
		$content_class = 'portfolio-three-sidebar';
	} elseif(get_post_meta($post->ID, 'pyre_portfolio_sidebar_position', true) == 'right') {
		$content_css = 'float:left;';
		$sidebar_css = 'float:right;';
		$content_class = 'portfolio-three-sidebar';
	} elseif(get_post_meta($post->ID, 'pyre_sidebar_position', true) == 'default') {
		$content_class = 'portfolio-three-sidebar';
		if($data['default_sidebar_pos'] == 'Left') {
			$content_css = 'float:right;';
			$sidebar_css = 'float:left;';
		} elseif($data['default_sidebar_pos'] == 'Right') {
			$content_css = 'float:left;';
			$sidebar_css = 'float:right;';
		}
	}
	?>
	<div id="content" class="portfolio portfolio-three <?php echo $content_class; ?>" style="<?php echo $content_css; ?>">
		<?php while(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<span class="entry-title" style="display: none;"><?php the_title(); ?></span>
			<span class="vcard" style="display: none;"><span class="fn"><?php the_author_posts_link(); ?></span></span>
			<span class="updated" style="display: none;"><?php the_time('c'); ?></span>
			<div class="post-content">
				<?php the_content(); ?>
				<?php wp_link_pages(); ?>
			</div>
		</div>
		<?php $current_page_id = $post->ID; ?>
		<?php endwhile; ?>
		<?php
		if(is_front_page()) {
			$paged = (get_query_var('page')) ? get_query_var('page') : 1;
		} else {
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		}
		/*$args = array(
			'post_type' => 'avada_portfolio',
			'paged' => $paged,
			'posts_per_page' => $data['portfolio_items'],
			'meta_key'		=> 'countdown',
			'orderby'		=> 'meta_value_num',
			'order'			=> 'DESC'
		);*/
		$args = array(
			'post_type' => 'avada_portfolio',
			'paged' => $paged,
			'posts_per_page' => $data['portfolio_items'],
			
		);
		$pcats = get_post_meta(get_the_ID(), 'pyre_portfolio_category', true);
		if($pcats && $pcats[0] == 0) {
			unset($pcats[0]);
		}
		if($pcats){
			$args['tax_query'][] = array(
				'taxonomy' => 'portfolio_category',
				'field' => 'ID',
				'terms' => $pcats
			);
		}
		$gallery = new WP_Query($args);
		if(is_array($gallery->posts) && !empty($gallery->posts)) {
			foreach($gallery->posts as $gallery_post) {
				$post_taxs = wp_get_post_terms($gallery_post->ID, 'portfolio_category', array("fields" => "all"));
				if(is_array($post_taxs) && !empty($post_taxs)) {
					foreach($post_taxs as $post_tax) {
						if(is_array($pcats) && !empty($pcats) && (in_array($post_tax->term_id, $pcats) || in_array($post_tax->parent, $pcats )) )  {
							$portfolio_taxs[urldecode($post_tax->slug)] = $post_tax->name;
						}

						if(empty($pcats) || !isset($pcats)) {
							$portfolio_taxs[urldecode($post_tax->slug)] = $post_tax->name;
						}
					}
				}
			}
		}

		$all_terms = get_terms('portfolio_category');
		if( !empty( $all_terms ) && is_array( $all_terms ) ) {
			foreach( $all_terms as $term ) {
				if( $portfolio_taxs[urldecode($term->slug)] ) {
					$sorted_taxs[urldecode($term->slug)] = $term->name;
				}
			}
		}

		$portfolio_taxs = $sorted_taxs;

		$portfolio_category = get_terms('portfolio_category');
		if( ! post_password_required($post->ID) ):
		if(is_array($portfolio_taxs) && !empty($portfolio_taxs) && get_post_meta($post->ID, 'pyre_portfolio_filters', true) != 'no'):
		?>
		<ul class="portfolio-tabs clearfix">
			<li class="active"><a data-filter="*" href="#"><?php echo __('All', 'Avada'); ?></a></li>
			<?php 			
			foreach($portfolio_taxs as $portfolio_tax_slug => $portfolio_tax_name):		
			?><li><a <?php
            if($portfolio_tax_slug=='the-biggest-jackpot')
				echo 'data-sort-by="wins"';
			else
				echo 'data-filter=".'.$portfolio_tax_slug.'"';
			?> href="#"><?php echo $portfolio_tax_name; ?></a>
           </li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>               
		<div class="portfolio-wrapper">
			<?php
			while($gallery->have_posts()): $gallery->the_post();
				if($pcats) {
					$permalink = tf_addUrlParameter(get_permalink(), 'portfolioID', $current_page_id);
				} else {
					$permalink = get_permalink();
				}
				if(has_post_thumbnail() || get_post_meta($post->ID, 'pyre_video', true)):
			?>
			<?php
			$item_classes = '';
			$item_cats = get_the_terms($post->ID, 'portfolio_category');
			if($item_cats):
			foreach($item_cats as $item_cat) {
				$item_classes .= urldecode($item_cat->slug) . ' ';
			}
			endif;
									
			//$pos1 = stripos($src,"oz");
			?>
			<div class="portfolio-item <?php echo $item_classes; ?>" style="width:285px;height:279px<?php 
			/*if ($pos1 === false)
				echo ' ;display:none';*/
				?>">
				<span class="vcard" style="display: none;"><span class="fn"><?php the_author_posts_link(); ?></span></span>
				<span class="updated" style="display: none;"><?php the_time('c'); ?></span>
				<?php if(has_post_thumbnail()): ?>
				<table border="0" cellpadding="0" cellspacing="0">
                <tr>
                	<td align="center" width="285" height="279" valign="top" background="<?php //the_post_thumbnail('portfolio-full'); 
					$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', false, '' );					
					echo $src[0];
					?>">
                    <div style="font-family:Arial, Helvetica, sans-serif; color:#FFF; font-size:27px; margin-top:112px; font-weight:bold;text-shadow: 0 -1px 0 #333,0 0 10px #98c8e5,0 0 10px #98c8e5,0 0 10px #98c8e5;" class="wins"><?php the_field('wins');?></div>
                    <div style="margin-top:3px">
                    <table border="0" cellpadding="0" cellspacing="0" align="center">
                	<tr>
                	<td ><?php 
					$chrono= get_field('countdown');
					if($chrono=='')
						$chrono='2013-08-16 15:00:00';
					echo do_shortcode('[tminus t="'.$chrono.'" style="carbonlite" omitweeks="true" jsplacement="inline" seconds="sec" minutes="min"/]');					
					?></td><td valign="top" style="padding-top: 10px;"><?php $flag = get_field('flag');
if( !empty($flag) ): ?>
	<img src="<?php echo $flag; ?>" width="32" />
<?php endif; ?></td></tr></table></div>   
					<div style="margin-top:0px;font-family:Arial, Helvetica, sans-serif; color:#FFF; font-size:16px; font-weight:bold;line-height: 25px;"><a href="http://<?php the_field('link_details'); ?>" style="color:#FFF">Details</a><br />
<a href="http://<?php the_field('link_last_results'); ?>" style="color:#FFF">Last Results</a><br />
<a href="http://<?php the_field('link_last_results'); ?>" ><img src="http://thetoplotto.com/wp-content/uploads/2014/08/play_now.png" /></a></div>                 
                    </td>
                </tr>
                </table>
				<?php endif; ?>
			</div>
			<?php endif; endwhile; ?>
		</div>
		<?php themefusion_pagination($gallery->max_num_pages, $range = 2); ?>
		<?php endif; // password check ?>
	</div>
	<div id="sidebar" style="<?php echo $sidebar_css; ?>"><?php generated_dynamic_sidebar(); ?></div>
<?php get_footer(); ?>