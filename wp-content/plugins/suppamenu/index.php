<?php

/*
Plugin Name: Suppa Menu
Plugin URI: http://vamospace.com
Description: Multifunctional WordPress Mega Menu . Please read the <a href="http://guides.vamospace.com/suppa_menu/">Guide</a>
Version: 1.8.9
Author: Sabri Taieb 
Author URI: http://vamospace.com
License: You should have purchased a license from http://codecanyon.net/
Copyright 2014  Sabri Taieb , Codetemp http://vamospace.com
*/

/** Defined **/
$suppa_settings = array
				(
					// Plugin Settings
					'plugin_id'			=> 'CTF_suppa_menu', // Don't ever ever ever change it
					'version'			=> '1.8.9',
					'guide'				=> 'http://guides.vamospace.com/suppa_menu/',
					'support_forum'		=> 'http://help.vamospace.com/',
					'image_resize'		=> true, // false to disable the image resizing 
					'tour'				=> false, // false to disable the tour
					'plugin_url'		=> plugins_url( '' , __FILE__ ) . '/' ,
					'icon_url'			=> plugins_url( '' , __FILE__ ) . '/standard/img/icon.png' ,
					'textdomain' 		=> 'suppa_menu', // Localisation ( translate )
					
					'default_skin_file'	=> 'demo.json', // default skin file ( .json is required )
					'plugin_dir'		=> plugin_dir_path( __FILE__ ),

					// Add Menu Page , Submenu Page Settins
					'menu_type'			=> 'menu_page',				// menu_page or submenu_page
					'page_title'		=> 'Suppa Menu Settings' ,	// page_title
					'menu_title'		=> 'Suppa Menu' ,		// menu_title
					'capability'		=> 'manage_options'	,		// capability

					// Framework Settings
					'framework_version'	=> '3.0.1',

					// Database Settings
					'groups'			=> array('style','settings') // Don't ever ever ever change it
				);

/** Files Required **/
require_once("core/class-all_fonts.php");
require_once('core/class-get_categories.php');
require_once('core/class-resize_thumbnails.php');
require_once('core/ctf_options.class.php');
require_once('core/ctf_setup.class.php');
require_once('core/array-fontAwesome.php');
require_once('standard/include/class-suppa_walkers.php');
require_once('standard/include/class-customs_to_files.php');
require_once('standard/include/class-tour.php');

/** Create [PLUGIN_NAME] CLASS **/
class codetemp_suppa_menu extends ctf_setup {

	/** Variables **/
	public $project_settings;
	static $plugin_activate;

	public function __construct( $project_settings = array() )
	{	
		$this->project_settings = $project_settings;
		self::$plugin_activate = $project_settings;

		/** This must be first , generate plugin_id **/
		parent::__construct();

		/** -------------------------------------------------------------------------------- **/

		/** Start Mega Menu walkers **/
		new suppa_walkers( $this->project_settings , $this->groups_db_offline );

		/** Add Support For WP 3+ Menus **/
		if( isset( $this->groups_db_offline['settings-theme_implement'] ) && $this->groups_db_offline['settings-theme_implement'] == 'on' )
		{
			register_nav_menus( array(
				'suppa_menu_location' => 'Suppa Menu Location'
			) );
		}

		/** -------------------------------------------------------------------------------- **/

		/** Thumbnail Resize with Timthumb-Alternative **/
		if( count( $this->groups_db_offline ) != 0 )
		{	
			$latest_posts_thumbs_size = new ctf_resize_thumbnails( $this->groups_db_offline['submenu-posts-post_width'], $this->groups_db_offline['submenu-posts-post_height'] );
			$latest_posts_thumbs_size->wp_actions();
			
			$mega_posts_thumbs_size = new ctf_resize_thumbnails( $this->groups_db_offline['submenu-megaposts-post_width'], $this->groups_db_offline['submenu-megaposts-post_height'] );		
			$mega_posts_thumbs_size->wp_actions();
			
			add_action( 'save_post', array( $this, 'save_post' ) );
		}
		
		/** -------------------------------------------------------------------------------- **/

		/** Admin : Load CSS & JS **/
		add_action( 'admin_enqueue_scripts' , array( $this , 'backend_css_js' ) );

		/** -------------------------------------------------------------------------------- **/

		/** Front-End : Load CSS & JS **/
		add_action( 'wp_head' , array( $this , 'frontend_head_style' ) , 1 );

		/** Front-End : Load CSS & JS **/
		add_action( 'wp_head' , array( $this , 'frontend_footer_scripts' ) , 2 );

		/** -------------------------------------------------------------------------------- **/
	
		$save_customs = new suppa_customs2files( $this->project_settings, $this->groups_db_offline );

		/** -------------------------------------------------------------------------------- **/
		if( $this->project_settings['tour'] )
		{
			$new_tour = new suppa_tour();
		}
	}

	
	/**
	 * Check if Thumbnails did not cut's
	 * 
	 * @param $post_id int
	 */
	public function save_post( $post_id )
	{
		$thumb_id = get_post_thumbnail_id( $post_id );
		
		$latest_posts_thumbs_size = new ctf_resize_thumbnails( $this->groups_db_offline['submenu-posts-post_width'], $this->groups_db_offline['submenu-posts-post_height'] );
		$latest_posts_thumbs_size->create_images( get_attached_file( $thumb_id ) );
			
		$mega_posts_thumbs_size = new ctf_resize_thumbnails( $this->groups_db_offline['submenu-megaposts-post_width'], $this->groups_db_offline['submenu-megaposts-post_height'] );			
		$mega_posts_thumbs_size->create_images( get_attached_file( $thumb_id ) );
	}

	
	/**
	 * Localisation ( WP Translate )
	 *
	 */
	public function translation_action()
	{
		load_plugin_textdomain( $this->project_settings['textdomain'] , false, basename( dirname( __FILE__ ) ) . '/languages' );
	}

		
	/**	
	 *
	 * Build & Display Settings Page .
	 *
	 * ( this function change with every new plugin or theme )
	 * 
	 */
	public function display_admin_page()
	{
		// Anouncements 
		//$this->add_box();
	
		// Header
		$header_desc 	= 'Suppa Menu ' . $this->project_settings['version'] . '<br/>Framework ' . $this->project_settings['framework_version'];
		$html_id 		= 'suppa_menu';
		echo $this->get_html_header( $header_desc , $html_id );

		// Nav & Pages 
		echo '<div class="codetemp_nav_pages_container">';

		// NAV (Main)
		echo $this->get_html_nav( array( 
									__( '<span class="icon ct-wrench"></span>Settings' , 'suppa_menu' )		=> array(
																												__( 'jQuery', 'suppa_menu' ),
																												__( 'Responsive', 'suppa_menu' ),
																												__( 'Sticky Menu', 'suppa_menu' ),
																												__( 'Suppport WP Menus', 'suppa_menu' ),
																												__( 'Search Form', 'suppa_menu' ),
																												__( 'Posts Settings', 'suppa_menu' ),
																												__( 'Logo Settings', 'suppa_menu' ),
																			),
									__( '<span class="icon ct-magic"></span>Menu Style' , 'suppa_menu' )	=> array(
																												__( 'General', 'suppa_menu' ),
																												__( 'Top Level Links', 'suppa_menu' ),
																												__( 'Current Top Level Link', 'suppa_menu' ),
																												__( 'Submenu General', 'suppa_menu' ),
																												__( '[Latest Posts]', 'suppa_menu' ),
																												__( '[Mega Posts]', 'suppa_menu' ),
																												__( '[Mega Links]', 'suppa_menu' ),																																																							
																												__( '[DropDown]', 'suppa_menu' ),
																												__( 'Search Form', 'suppa_menu' ),
																			),
									__( '<span class="icon ct-magic"></span>Menu Icons Style' , 'suppa_menu' )	=> array(
																												__( 'Top Level Icons', 'suppa_menu' ),
																												__( '[Mega Links] Title Icon', 'suppa_menu' ),																												
																												__( '[Mega Links] Links Icon', 'suppa_menu' ),
																												__( '[Dropdown] Icons', 'suppa_menu' ),																																																											
																												__( 'Social Media', 'suppa_menu' ),
																			),
									__( '<span class="icon ct-magic"></span>Responsive Style' , 'suppa_menu' )	=> array(),
									__( '<span class="icon ct-magic"></span>Responsive Icons Style' , 'suppa_menu' )	=> array(),
									__( '<span class="icon ct-magic"></span>Custom CSS' , 'suppa_menu' )	=> array(),

								) 
		);

		// Pages
		echo '		<!-- Pages Container -->
					<div class="codetemp_pages_container fl">';
					?>
						<!-- Page 1 -->
						<div class="codetemp_page" id="codetemp_page_1">
							<?php require_once('standard/include/settings-general.php') ?>							
						</div><!--page_1-->	

						<!-- Page 1 -->
						<div class="codetemp_page" id="codetemp_page_1_1">
							<?php require_once('standard/include/settings-jquery.php') ?>							
						</div><!--page_1-->	

						<!-- Page 1 -->
						<div class="codetemp_page" id="codetemp_page_1_2">
							<?php require_once('standard/include/settings-rwd.php') ?>							
						</div><!--page_1-->	

						<!-- Page 1 -->
						<div class="codetemp_page" id="codetemp_page_1_3">
							<?php require_once('standard/include/settings-sticky.php') ?>							
						</div><!--page_1-->	

						<!-- Page 1 -->
						<div class="codetemp_page" id="codetemp_page_1_4">
							<?php require_once('standard/include/settings-theme_implementation.php') ?>							
						</div><!--page_1-->	

						<!-- Page 1 -->
						<div class="codetemp_page" id="codetemp_page_1_5">
							<?php require_once('standard/include/settings-search_form.php') ?>							
						</div><!--page_1-->	

						<!-- Page 1 -->
						<div class="codetemp_page" id="codetemp_page_1_6">
							<?php require_once('standard/include/settings-posts.php') ?>							
						</div><!--page_1-->	

						<!-- Page 1 -->
						<div class="codetemp_page" id="codetemp_page_1_7">
							<?php require_once('standard/include/settings-logo.php') ?>							
						</div><!--page_1-->	

						<!-- Page 2 -->
						<div class="codetemp_page" id="codetemp_page_2">
							<?php require_once('standard/include/style.php') ?>
						</div>

						<!-- Page 2.1 -->
						<div class="codetemp_page" id="codetemp_page_2_1">
							<?php require_once('standard/include/style-general.php') ?>
						</div>

						<!-- Page 2.1 -->
						<div class="codetemp_page" id="codetemp_page_2_2">
							<?php require_once('standard/include/style-top_level.php') ?>
						</div>

						<!-- Page 2.1 -->
						<div class="codetemp_page" id="codetemp_page_2_3">
							<?php require_once('standard/include/style-current_top_link.php') ?>
						</div>

						<!-- Page 2.1 -->
						<div class="codetemp_page" id="codetemp_page_2_4">
							<?php require_once('standard/include/style-submenu_general.php') ?>
						</div>

						<!-- Page 2.1 -->
						<div class="codetemp_page" id="codetemp_page_2_5">
							<?php require_once('standard/include/style-submenu_posts.php') ?>
						</div>

						<!-- Page 2.1 -->
						<div class="codetemp_page" id="codetemp_page_2_6">
							<?php require_once('standard/include/style-megaposts.php') ?>
						</div>

						<!-- Page 2.1 -->
						<div class="codetemp_page" id="codetemp_page_2_7">
							<?php require_once('standard/include/style-submenu_links.php') ?>
						</div>

						<!-- Page 2.1 -->
						<div class="codetemp_page" id="codetemp_page_2_8">
							<?php require_once('standard/include/style-dropdown.php') ?>
						</div>

						<!-- Page 2.10 -->
						<div class="codetemp_page" id="codetemp_page_2_9">
							<?php require_once('standard/include/style-search_form.php') ?>
						</div>

						<!-- Page 3 -->
						<div class="codetemp_page" id="codetemp_page_3_1">
							<?php require_once('standard/include/style-top_level_icons.php') ?>
						</div>
						<div class="codetemp_page" id="codetemp_page_3_2">
							<?php require_once('standard/include/style-submenu_mega_title_icons.php') ?>
						</div>
						<div class="codetemp_page" id="codetemp_page_3_3">
							<?php require_once('standard/include/style-submenu_mega_link_icons.php') ?>
						</div>
						<div class="codetemp_page" id="codetemp_page_3_4">
							<?php require_once('standard/include/style-dropdown_icons.php') ?>
						</div>
						<div class="codetemp_page" id="codetemp_page_3_5">
							<?php require_once('standard/include/style-social.php') ?>
						</div>

						<!-- Page 4 -->
						<div class="codetemp_page" id="codetemp_page_4">
							<?php require_once('standard/include/style-rwd.php') ?>
						</div>

						<!-- Page 5 -->
						<div class="codetemp_page" id="codetemp_page_5">
							<?php require_once('standard/include/style-rwd_icons.php') ?>
						</div>

						<!-- Page 6 -->
						<div class="codetemp_page" id="codetemp_page_6">
							<?php require_once('standard/include/style-custom.php') ?>
						</div>

					<?php
		echo		'</div><!--codetemp_pages_container-->
				
					<div class="clearfix"></div>

			  </div><!--codetemp_nav_pages_container-->';

		// Footer
		echo $this->get_html_footer();
	}



	/**
	 *
	 *	Load Admin : CSS & JS
	 *
	 */
	public function backend_css_js($hook)
	{
		if( 'toplevel_page_CTF_suppa_menu' == $hook )
		{
			wp_enqueue_style('suppa_admin_menus_style', $this->project_settings['plugin_url'] . '/standard/css/suppa_admin_framework.css' );
			wp_enqueue_script('suppa_admin_load_skin', $this->project_settings['plugin_url'] . '/standard/js/load_skin.js' );
		}

		if( 'nav-menus.php' == $hook )
		{
			wp_enqueue_style('suppa_admin_menus_style', $this->project_settings['plugin_url'] . '/standard/css/suppa_admin_menus.css' );
			wp_enqueue_style('suppa_admin_menus_script', $this->project_settings['plugin_url'] . '/standard/js/suppa_admin.js' , array( 'jquery' ) );
		}

		wp_enqueue_style('suppa_tour', $this->project_settings['plugin_url'] . '/standard/css/suppa_tour.css' );
		wp_enqueue_style('suppa_admin_menus_script', $this->project_settings['plugin_url'] . '/standard/js/suppa_tour.js' , array( 'jquery' ) );
	
	}


	/**
	 *
	 *	Front-End : Head CSS
	 *
	 */
	public function frontend_head_style()
	{
		/** Main Style **/
		wp_enqueue_style('suppamenu_style' , $this->project_settings['plugin_url'] . 'standard/css/suppa_frontend_style.css', false , $this->project_settings['version'] , 'screen' );
		
		$style_file = get_option('suppa_custom_style_file');
		if( is_ssl() )
			$style_file = str_replace('http://', 'https://', $style_file );
		
		wp_enqueue_style('suppamenu_custom_style' , $style_file , false , $this->project_settings['version'] , 'screen');
		
		/** Font Awesome **/
		wp_enqueue_style( 'suppa_frontend_fontAwesome' , $this->project_settings['plugin_url'] . 'standard/css/fontAwesome/style-min.css' , false , $this->project_settings['version'] , 'screen' );

		/** Hover.css Effects **/
		wp_enqueue_style( 'suppa_frontend_hoverCSS' , $this->project_settings['plugin_url'] . 'standard/css/hover-master/hover-min.css' , false , $this->project_settings['version'] , 'screen' );

	}


	/**
	 *
	 *	Front-End : Footer Scripts
	 *
	 */
	public function frontend_footer_scripts()
	{
		$js_file = get_option('suppa_js_settings_file');
		if( is_ssl() )
			$js_file = str_replace('http://', 'https://', $js_file );

		wp_enqueue_script('suppamenu_js_settings_file' , $js_file , array() , $this->project_settings['version'] , true );
		wp_enqueue_script('suppamenu_frontend_script' , $this->project_settings['plugin_url'] . 'standard/js/suppa_frontend.min.js' , array('jquery' , 'suppamenu_js_settings_file' , 'jquery-ui-core' , 'jquery-effects-core' ) , $this->project_settings['version'] , true );

	}

	static function plugin_install() 
	{	
		if( !get_option(self::$plugin_activate['plugin_id'].'_settings' ) )
		{
			$default_skin_file 	= self::$plugin_activate['plugin_dir'] . 'standard/css/skins/' . self::$plugin_activate['default_skin_file'];	
			$str_data 			= file_get_contents( $default_skin_file );
			$all_settings 		= json_decode($str_data,true);
			update_option( self::$plugin_activate['plugin_id'].'_settings' , $all_settings );
    	}
    }


}// end class


/** Show Time **/
$suppa_menu_start = new codetemp_suppa_menu ( $suppa_settings );
/** Plugin Activation Hook **/
register_activation_hook( __FILE__, array( 'codetemp_suppa_menu', 'plugin_install' ) );

/** Theme Implementation for WP 3+ Menus **/
if( !function_exists('suppa_implement') )
{
	function suppa_implement()
	{
		$args = array();

		$args['walker'] 				= new suppa_menu_walker();
		$args['container_class'] 		= ' suppaMenu_wrap';
		$args['menu_class']				= ' suppaMenu';
		$args['items_wrap']				= '<div id="%1$s" class="%2$s">%3$s</div>';
		$args['depth']					= 4;
		$args['theme_location']			= 'suppa_menu_location';

		wp_nav_menu( $args );
	}
} 

/** wooThemes canvas support**/
if ( ! function_exists( 'woo_nav' ) ) {
function woo_nav() {
	global $woo_options;
	woo_nav_before();
	if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary-menu' ) ) 
	{
		wp_nav_menu( array( 'theme_location' => 'primary-menu' ) );
	}
	woo_nav_after();
} // End woo_nav()
}



/** Shortcode For : Login Form **/
// http://pippinsplugins.com/wordpress-login-form-short-code/
function suppa_login_sc( $atts, $content = null ) {
 
	extract( shortcode_atts( array(
      'redirect' => ''
      ), $atts ) );
 

	if($redirect) {
		$redirect_url = $redirect;
	} else {
		$redirect_url = get_permalink();
	}
	$form = wp_login_form(array('echo' => false, 'redirect' => $redirect_url ));

	return $form;
}
add_shortcode('suppa_login_sc', 'suppa_login_sc');

/** Shortcode for the menu itself **/
function suppa_menu_func( $atts ) {
    suppa_implement();
}
add_shortcode('suppa_menu', 'suppa_menu_func');

?>