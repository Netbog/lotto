<?php

echo '<h3 class="ctf_page_title">' . __('Load a Skin','suppa_menu') . '</h3>';

$suppa_skins = array(
	array(
			'title' => 'Default',
			'file'	=> 'default.json',
			'class'	=> 'suppa_skin_default'
		),
	array(
			'title' => 'RedRosa',
			'file'	=> 'redrosa.json',
			'class'	=> 'suppa_skin_redrosa'
		),
	array(
			'title' => 'Calvarine',
			'file'	=> 'calvarine.json',
			'class'	=> 'suppa_skin_calvarine'
		),
	array(
			'title' => 'Cyberia',
			'file'	=> 'cyberia.json',
			'class'	=> 'suppa_skin_cyberia'
		),
	array(
			'title' => 'Jallon',
			'file'	=> 'jallon.json',
			'class'	=> 'suppa_skin_jallon'
		),
	array(
			'title' => 'Bastlow',
			'file'	=> 'bastlow.json',
			'class'	=> 'suppa_skin_bastlow'
		),
	array(
			'title' => 'Wally',
			'file'	=> 'wally.json',
			'class'	=> 'suppa_skin_wally'
		),
	array(
			'title' => 'Demo',
			'file'	=> 'demo.json',
			'class'	=> 'suppa_skin_demo'
		),
	array(
			'title' => 'Ubuntus',
			'file'	=> 'ubuntus.json',
			'class'	=> 'suppa_skin_ubuntus'
		),
	array(
			'title' => 'Ubuntus2',
			'file'	=> 'ubuntus2.json',
			'class'	=> 'suppa_skin_ubuntus2'
		),
	array(
			'title' => 'Light',
			'file'	=> 'light.json',
			'class'	=> 'suppa_skin_light'
		),
	array(
			'title' => 'Redow',
			'file'	=> 'redow.json',
			'class'	=> 'suppa_skin_redow'
		),
);

$suppa_skin_selected = ( isset( $this->groups_db_offline['skin_selected'] ) ) ? $this->groups_db_offline['skin_selected'] : 'default.css'; 
echo '<br/><br/><h3 class="ctf_option_title">'.__('Select a Skin','suppa_menu').'</h3><ul class="suppa_skins_list">';
foreach ( $suppa_skins as $skin ) 
{
	if( $skin['file'] == $suppa_skin_selected )
		echo '<li data-skin_file="'.$skin['file'].'" class="'.$skin['class'].' suppa_skin_selected"><span>'.$skin['title'].'</span></li>';
	else
		echo '<li data-skin_file="'.$skin['file'].'" class="'.$skin['class'].'"><span>'.$skin['title'].'</span></li>';
}
echo '</ul><div class="clearfix"></div><br/><br/>';

$this->add_hidden(
	array(
		'group_id'			=> 'style', 		
		'option_id'			=> 'skin_selected', 		
		'value'				=> 'default.json',
		'class'				=> 'skin_holder'
	)
);

$this->add_hidden(
	array(
		'group_id'			=> 'style', 		
		'option_id'			=> 'skin_url', 		
		'value'				=> $this->project_settings['plugin_url'].'standard/css/skins/',
		'class'				=> 'skin_url',
		'fetch'				=> 'no'
	)
);

echo '<button class="button button-large" id="suppa_load_skin">' . __('Load a Skin','suppa_menu') . '</button>' ;
