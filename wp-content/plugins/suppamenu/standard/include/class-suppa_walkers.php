<?php 

/**
 * This file holds various classes and methods necessary to edit the wordpress menu.
 *
 * @package 	CTFramework
 * @author		Sabri Taieb ( codezag )
 * @copyright	Copyright (c) Sabri Taieb
 * @link		http://codetemp.com
 * @since		Version 1.0
 *
 */

/**
 * This class contains various methods necessary to create mega menus in the backend
 * @package CTFramework
 * 
 */
if( !class_exists( 'suppa_walkers' ) )
{
	class suppa_walkers
	{

		static $project_settings;
		static $offline_db;

		/**
		 * Constructor
		 * @package CTFramework
		 *
		 */
		public function __construct( $project_settings , $offline_db )
		{
			/** Variables **/
			self::$project_settings = $project_settings;
			self::$offline_db 		= $offline_db;

		
			if( count( self::$offline_db ) == 0 )
			{
				self::$offline_db['settings-responsive_text'] = 'Menu';
				self::$offline_db['logo_enable'] = 'off';
				self::$offline_db['rwd_logo_enable'] = 'off';
				self::$offline_db['submenu-megaposts-post_width'] = '200px';
				self::$offline_db['submenu-megaposts-post_height'] = '160px';
				self::$offline_db['posts_img_effect'] = 'none';
				self::$offline_db['submenu-posts-post_width'] = '200px';
				self::$offline_db['submenu-posts-post_height'] = '160px';
				self::$offline_db['latest_posts_view_all'] = 'View All...';
			}

			/** Load style & javascript to admin : nav-menus.php only **/
			add_action('admin_menu', array($this,'load_menus_css') , 9);
			add_action( 'admin_print_styles-nav-menus.php', array( $this , 'load_menus_js' ) , 3000 ); 

			/** Replace the selected menu args **/
			add_filter( 'wp_nav_menu_args', array( $this,'replace_args'), 3000);

			/** add new options to the walker **/
			add_filter( 'wp_edit_nav_menu_walker', array( $this,'replace_backend_walker') , 3000 );

			/** save suppa menu new options **/
			add_action( 'wp_update_nav_menu_item', array( $this,'update_menu'), 101, 3);

			/** Add WP Edior & Font Awesome Widgets on the Footer **/
			add_action( 'admin_footer', array( $this , 'add_widgets' ) );

			/** Ajax : Save Menu Location **/
			add_action( 'wp_ajax_suppamenu_save_menu_location' , array( $this , 'save_menu_location' ) ); 
		
			add_action( 'admin_head', array( $this , 'add_accordion_metabox' ) );

			/** Swith To Suppa Walker Axtion **/
			add_action( 'wp_ajax_suppa_switch_menu_walker' , array( $this , 'switch_menu_walker' ) );

		}


		/**
		 *
		 * Add JS & CSS only on nav-menus.php
		 * @package CTFramework
		 *
		 */
		function load_menus_css( )
		{
			if( basename( $_SERVER['PHP_SELF'] ) == "nav-menus.php" )
			{
				wp_enqueue_style ( 'suppa_menu_admin_menu_css', plugins_url('../css/' , __FILE__ ). 'suppa_admin_menus.css');
				wp_enqueue_style ( 'suppa_menu_admin_fontAwesome', plugins_url('../css/fontAwesome/' , __FILE__ ). 'style.css');
			}
		}
		function load_menus_js( ) 
		{
			if( basename( $_SERVER['PHP_SELF'] ) == "nav-menus.php" )
			{
				// WP 3.5+ 
				// Enqueue Media uploader scripts and environment [ wp_enqueue_media() ].
				// Strongly suggest to use this function on the admin_enqueue_scripts action hook. Using it on admin_init hook breaks it
				// How To : http://stackoverflow.com/questions/13847714/wordpress-3-5-custom-media-upload-for-your-theme-options
				// Don't Foooooooooooooooooorget to  array('jquery' , 'media-upload' , 'thickbox')  to the enqueue
				wp_enqueue_media();
				wp_enqueue_script( 'suppa_menu_admin_js' , plugins_url('../js/'  , __FILE__ ).'suppa_admin.js',  array('jquery' , 'media-upload' , 'thickbox' , 'jquery-ui-core' , 'jquery-ui-draggable' , 'jquery-ui-droppable' , 'jquery-ui-sortable' ), '1.0.0', true );
			}
		}

		/**
		 *
		 * Add Select Location Meta Box
		 * @package CTFramework
		 *
		 */
		function add_accordion_metabox()
		{ 
			add_meta_box( 'nav-menu-theme-suppa-location', __( 'Select Menu Location' , 'suppa_menu' ), array( $this , 'display_select_location_meta_box' ) , 'nav-menus', 'side', 'high' );
		}


		/**
		 *
		 * Select Location Meta Box Render
		 * @package CTFramework
		 *
		 */
		function display_select_location_meta_box()
		{
			// Get Settings
			$settings = array();

			// Add Accordion Menu Locations
			$menu_locations = get_registered_nav_menus();
			$menus = get_terms('nav_menu');

			if( get_option('suppa_menu_settings') )
			{
				$saved_locations = get_option('suppa_menu_settings');
			}

			echo '
			<p>
				<div id="suppa_menu_location_selected">
				';
					foreach ($menu_locations as $key => $value)
					{
						if( in_array($key, $saved_locations) )
						{
							echo '<input type="checkbox" value="'.$key.'" checked >&nbsp;&nbsp;&nbsp;'.$value; 
						}
						else
						{
							echo '<input type="checkbox" value="'.$key.'" >&nbsp;&nbsp;&nbsp;'.$value; 
						}
						echo '<br/>';
					}
			echo '
				</div><!--suppa_menu_location_selected-->
			</p>			
			<p>
				<span>
					<input type="submit" class="button-primary" value="Save" id="admin_suppa_save_menu_location">
					<input type="hidden" value="'.wp_create_nonce("suppa_menu_location_nonce").'" id="admin_suppa_save_menu_location_nonce">

				</span>
			</p>
			<br/><br/>
			';

		}


		/**
		 *
		 * Add WP Edior & Font Awesome Widgets on the Footer
		 * @package CTFramework
		 *
		 */
		function add_widgets() 
		{
			global $fontAwesome;

			if( basename( $_SERVER['PHP_SELF'] ) == "nav-menus.php" )
			{
				// Add Widgets
				echo '
				<input type="hidden" id="admin_suppa_plugin_url" value="'.plugins_url( '../js/tinymce/' , __FILE__ ).'" />
				<div class="era_admin_widgets_container" >
					<div class="era_admin_widget_box suppa_wp_editor_container" >
						
						<div class="era_admin_widget_box_header">
							<span>WP Editor</span>
							<a>x</a>
						</div>

						<div class="era_admin_widget_box_inside" >

							';
							
							wp_editor( '', 'suppa_wp_editor_the_editor', $settings = array() );
							
							echo '							
							<div class="admin_suppa_clearfix"></div>
						</div>

						<div class="era_admin_widget_box_footer">
							<button class="era_admin_widgets_container_button admin_suppa_getContent_button">Add</button>
						</div>

					</div>
					';

					

				echo '
					<div class="era_admin_widget_box suppa_fontAwesome_container" >
						
						<div class="era_admin_widget_box_header">
							<span>Select an Icon</span>
							<a>x</a>
						</div>

						<div class="era_admin_widget_box_inside" >';

						foreach ( $fontAwesome as $icon ) 
						{
							echo '
								<span class="admin_suppa_fontAwesome_icon_box">
									<span aria-hidden="true" class="'.$icon.'"></span>
								</span>
							';

						}

				echo '
							<div class="admin_suppa_clearfix"></div>
						</div>

						<div class="era_admin_widget_box_footer">
							<button class="era_admin_widgets_container_button admin_suppa_addIcon_button">Add</button>
						</div>

					</div>
				</div>';
			}
		}


		/**
		 *
		 * Ajax : Save Menu Location
		 * @package CTFramework
		 *
		 */
		function save_menu_location()
		{
			check_ajax_referer( 'suppa_menu_location_nonce', 'nonce' );
			
			$locations	= $_POST['location'];
			$locations = explode(",",$locations);

			if( get_option('suppa_menu_settings') )
			{ update_option('suppa_menu_settings', $locations ); }
			else 
			{ add_option( 'suppa_menu_settings', $locations ); }

			die( __("Location Saved","suppa_menu") );
		}


		/**
		 *
		 * Replace the selected menu args
		 * @package CTFramework
		 *
		 */
		function replace_args($args){
							
			if( get_option('suppa_menu_settings') )
			{

				$saved_locations 	= get_option('suppa_menu_settings');

				/** Menu, Logo, Responsive **/
				$site_url = site_url();
				if( is_multisite() )
				{
					$site_url = network_site_url();
				}

				/** RWD **/
				$rwd_menu_text = self::$offline_db['settings-responsive_text'];
				if( function_exists('icl_t') )
				{
					$rwd_menu_text = icl_t('admin_texts_plugin_CTF_suppa_menu', '[CTF_suppa_menu__group__settings]settings-responsive_text', 'Menu');
				}

				/** LOGO **/
				$nlogo = '';
				if( suppa_walkers::$offline_db['logo_enable'] == 'on' )
				{
					$nlogo = '<a href="' . $site_url . '" class="suppa_menu_logo" ><img src="' . suppa_walkers::$offline_db['logo_src'] . '" data-retina_logo="' . suppa_walkers::$offline_db['logo_retina_src'] . '" /></a>';
				}

				$rwd_logo = "";
				if( suppa_walkers::$offline_db['rwd_logo_enable'] == 'on' )
				{
					$rwd_logo = '<a href="' . $site_url . '" class="suppa_rwd_logo" ><img src="' . suppa_walkers::$offline_db['rwd_logo_src'] . '" data-retina_logo="' . suppa_walkers::$offline_db['rwd_logo_retina_src'] . '" /></a>';
				}

				$rwd_wrap = '
				<div class="suppaMenu_rwd_wrap" >
					<div class="suppa_rwd_top_button_container">
						'.$rwd_logo.'
						<span class="suppa_rwd_button"><span aria-hidden="true" class="suppa-reorder"></span></span>
						<span class="suppa_rwd_text">' . $rwd_menu_text . '</span>
					</div>
					<div class="suppa_rwd_menus_container" ></div>
				</div>
				';

				if( array_key_exists('location', $saved_locations ) )
				{
					$location 	= $saved_locations['location'];
					if( $args['theme_location'] == $location )
					{
						if( class_exists('suppa_menu_walker') )
						{
							$args['walker'] 				= new suppa_menu_walker();
							$args['container_class'] 		= 'suppaMenu_wrap';
							$args['menu_class']				= 'suppaMenu';
							$args['items_wrap']				= $nlogo . '<div id="%1$s" class="%2$s">%3$s</div>' . $rwd_wrap;
							$args['depth']					= 4;
							$args['container']				= 'div';
						}
					}	
				}
				else
				{
					if( in_array($args['theme_location'], $saved_locations) )
					{
						if( class_exists('suppa_menu_walker') )
						{
							$args['walker'] 				= new suppa_menu_walker();
							$args['container_class'] 		= 'suppaMenu_wrap';
							$args['menu_class']				= 'suppaMenu';
							$args['items_wrap']				= $nlogo . '<div id="%1$s" class="%2$s">%3$s</div>' . $rwd_wrap;
							$args['depth']					= 4;
							$args['container']				= 'div';
						}
					}
				}
			}

			/*
			echo '<pre>';
				print_r( $args );
			echo '<pre>';
			*/

			return $args;
		}


		/**
		 *
		 * Tells wordpress to use our backend walker instead of the default one
		 * @package CTFramework
		 *
		 */
		function replace_backend_walker($name)
		{
			return 'suppa_menu_backend_walker';
		}



		/*
		 * Save and Update the Custom Navigation Menu Item Properties by checking all $_POST vars with the name of $check
		 * @param int $menu_id
		 * @param int $menu_item_db
		 */
		function update_menu($menu_id, $menu_item_db)
		{
			$all_keys = array( 
								'menu_type' ,
								'dropdown_width',
								'logo_image',
								'logo_image_retina',
								'links_fullwidth',
								'links_width',
								'links_align',
								'links_column_width',
								'html_fullwidth',
								'html_width',
								'html_align',
								'html_content',
								'link_position',
								'link_icon_type',
								'link_icon_image',
								'link_icon_image_hover',
								'link_icon_image',
								'link_icon_fontawesome',
								'link_icon_fontawesome_size',
								'posts_taxonomy',
								'posts_category',
								'posts_number',
								'search_text',
								'logo_image_height',
								'logo_image_width',
								'link_icon_image_height',
								'link_icon_image_width',
								'link_user_logged',
								'mega_posts_category',
								'mega_posts_taxonomy',
								'mega_posts_number'
							);

			foreach ( $all_keys as $key )
			{
				if(!isset($_POST['menu-item-suppa-'.$key][$menu_item_db]))
				{
					$_POST['menu-item-suppa-'.$key][$menu_item_db] = "";
				}

				$value = $_POST['menu-item-suppa-'.$key][$menu_item_db];
				update_post_meta( $menu_item_db, '_menu-item-suppa-'.$key, $value );
			}
		}


		/** 
		 * Very Important .
		 * Replace The WP add menu item by AJAX
		 * This Will be remove when they add an action in the future
		 *
		 */
		public function switch_menu_walker()
		{
			if ( ! current_user_can( 'edit_theme_options' ) )
			die('-1');

			check_ajax_referer( 'add-menu_item', 'menu-settings-column-nonce' );
		
			require_once ABSPATH . 'wp-admin/includes/nav-menu.php';
		
			$item_ids = wp_save_nav_menu_items( 0, $_POST['menu-item'] );
			if ( is_wp_error( $item_ids ) )
				die('-1');
		
			foreach ( (array) $item_ids as $menu_item_id ) {
				$menu_obj = get_post( $menu_item_id );
				if ( ! empty( $menu_obj->ID ) ) {
					$menu_obj = wp_setup_nav_menu_item( $menu_obj );
					$menu_obj->label = $menu_obj->title; // don't show "(pending)" in ajax-added items
					$menu_items[] = $menu_obj;
				}
			}
		
			if ( ! empty( $menu_items ) ) {
				$args = array(
					'after' => '',
					'before' => '',
					'link_after' => '',
					'link_before' => '',
					'walker' => new suppa_menu_backend_walker,
				);
				echo walk_nav_menu_tree( $menu_items, 0, (object) $args );
			}
			
			die('end');
		}
	}
}



if( !class_exists( 'suppa_menu_walker' ) )
{

	/**
	 * This walker is for the frontend
	 */
	class suppa_menu_walker extends Walker {
		/**
		 * @see Walker::$tree_type
		 * @var string
		 */
		var $tree_type = array( 'post_type', 'taxonomy', 'custom' );

		/**
		 * @see Walker::$db_fields
		 * @todo Decouple this.
		 * @var array
		 */
		var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

		/**
		 * @var string $menu_type
		 */
		var $menu_type = '';

		/**
		 * @var string $menu_key
		 */
		var $menu_key = '_menu-item-suppa-';

		/**
		 * @var int $top_level_counter
		 */
		var $top_level_counter = 0;

		/**
		 * @var int $top_level_counter
		 */
		var $dropdown_first_level_conuter = 0;

		/**
		 * @var int $top_level_counter
		 */
		var $dropdown_second_level_conuter = 0;


		/**
		 * @var int $column
		 */
		var $column = 0;

		
		/**
		 * @var string $dropdown_width
		 */
		var $dropdown_width = "180px";


		/**
		 * @var string $dropdown_position
		 */
		var $dropdown_position = "left";


		/**
		 * @var string $dropdown_position
		 */
		var $links_column_width = "180px";


		/**
		 * @var string $dropdown_position
		 */
		var $mega_posts_items = array();


		/**
		 * @var string $dropdown_position
		 */
		var $suppa_item_id = 0;


		/**
		 * @see Walker::start_lvl()
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int $depth Depth of page. Used for padding.
		 */
		function start_lvl(&$output, $depth = 0, $args = array()) 
		{
			// DropDown
			if( $this->menu_type == 'dropdown' )
			{
				if( $depth == 0 )
				{
					$output = str_replace("<span class=\"suppa_ar_arrow_down_".$this->top_level_counter."\"></span>", '<span class="era_suppa_arrow_box ctf_suppa_fa_box_top_arrow"><span aria-hidden="true" class="suppa-caret-down"></span></span>' , $output );
				}

				$css_left = '0px';
				if( $depth != 0 )
				{
					$css_left = $this->dropdown_width;
					
				}

				if( $depth == 1 )
				{
					$output = str_replace("<span class=\"suppa_ar_arrow_right_".$this->dropdown_first_level_conuter.'_'.$depth."\"></span>", '<span class="era_suppa_arrow_box"><span aria-hidden="true" class="suppa-caret-right"></span></span>' , $output );
				}

				if( $depth == 2 )
				{
					$output = str_replace("<span class=\"suppa_ar_arrow_right_".$this->dropdown_second_level_conuter.'_'.$depth."\"></span>", '<span class="era_suppa_arrow_box"><span aria-hidden="true" class="suppa-caret-right"></span></span>' , $output );
				}

				if( $this->dropdown_position == "none" )
					$this->dropdown_position = "left";
				
				$output .= '<div class="suppa_submenu suppa_submenu_'.$depth.'" style="width:'.$this->dropdown_width.';'.$this->dropdown_position.':'.$css_left.';" >';
			}

			// mega_posts
			if( 'mega_posts' == $this->menu_type )
			{
				if( $depth == 0 )
				{
					$output .= '<div class="suppa_mega_posts_categories" >';
				}

			}
		}


		/**
		 * @see Walker::end_lvl()
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int $depth Depth of page. Used for padding.
		 */
		function end_lvl(&$output, $depth = 0, $args = array()) 
		{	
			global $wp_query;

			if( $this->menu_type == 'dropdown' )
			{
				$output .= '</div>';
			}

			// mega_posts
			if( 'mega_posts' == $this->menu_type )
			{
				if( $depth == 0 )
				{
					$output .= '</div><!--suppa_mega_posts_categories-->';
				}

			}
		}

		/**
		 * @see Walker::start_el()
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param object $item Menu item data object.
		 * @param int $depth Depth of menu item. Used for padding.
		 * @param int $current_page Menu item ID.
		 * @param object $args
		 */
		function start_el(&$output, $item, $depth = 0, $args = array(), $current_object_id = 0) {
			global $wp_query;

			// Link Attributes
			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			
			// Link Icon
			$icon_type 	= get_post_meta( $item->ID, $this->menu_key.'link_icon_type', true);
			$link_title	= $item->title;
			$icon 		= get_post_meta( $item->ID, $this->menu_key.'link_icon_image', true);
			$icon_hover = get_post_meta( $item->ID, $this->menu_key.'link_icon_image_hover', true);
			$icon_only 	= get_post_meta( $item->ID, $this->menu_key.'link_icon_only', true);
			$FA_icon 	= get_post_meta( $item->ID, $this->menu_key.'link_icon_fontawesome', true);
			$link_html 	= "";

			// Link Classes
			$class_names = '';
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
			$class_names = esc_attr( $class_names );
			if( $depth === 0 )
			{
				$class_names .= ' suppa_top_level_link';
			}

			// Item Description
			$description  	= ! empty( $item->description ) ? '<span class="suppa_item_desc">'.$item->description.'</span>' : '';
			$description 	= '';

			// Item Icon
			if( $icon_type == "upload" )
			{
				if( $icon != "" )
				{
					$check_retina_icon = ( $icon_hover != "" ) ? $icon_hover : $icon;
					$link_html = '<img class="suppa_upload_img suppa_UP_icon" src="'.$icon.'" alt="'.$link_title.'" data-icon="'.$icon.'" data-retina="'.$check_retina_icon.'" ><span class="suppa_item_title">'.$link_title.$description.'</span>';
				}
				else
				{
					$link_html = '<span class="suppa_item_title">'.$link_title.$description.'</span>';
				}
			}
			else if( $icon_type == "fontawesome" )
			{
				if( $FA_icon != "" )
				{ 
					$link_html = '<span class="ctf_suppa_fa_box suppa_FA_icon"><span aria-hidden="true" class="'.$FA_icon.'" ></span></span><span class="suppa_item_title">'.$link_title.$description.'</span>';
				}
				else
				{
					$link_html = '<span class="suppa_item_title">'.$link_title.$description.'</span>';
				}
			}
			else
			{
				$link_html = '<span class="suppa_item_title">'.$link_title.$description.'</span>';
			}

			// If Level 0
			if( $depth === 0 )
			{
				$this->top_level_counter += 1;
				$this->menu_type = get_post_meta( $item->ID, $this->menu_key.'menu_type', true);
				
				$this_item_position = get_post_meta( $item->ID, $this->menu_key.'link_position', true);
				$this_item_position_class = ' suppa_menu_position_'.$this_item_position.' ';
				$class_names .= $this_item_position_class;

				$this_item_position_css = ( $this_item_position == "right" || $this_item_position == "none" ) ? ' float:none; ' : ' float:left; ';

				// Dropdown
				if( 'dropdown' == $this->menu_type )
				{	
					$user_logged_in_out = get_post_meta( $item->ID, $this->menu_key.'link_user_logged', true);
					$display_item = ' display:none !important; ';
					if( $user_logged_in_out == 'both' )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_in' && is_user_logged_in() )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_out' && !is_user_logged_in() )
						{ $display_item = ''; }
					
					$this->dropdown_width = get_post_meta( $item->ID, $this->menu_key.'dropdown_width', true);
					$this->dropdown_position = get_post_meta( $item->ID, $this->menu_key.'link_position', true);

					$arrow = '';
					$has_arrow = '';
					if( in_array( 'menu-item-has-children' , $item->classes ) )
					{
						$has_arrow = ' suppa_top_links_has_arrow ';
						$arrow = '<span class="era_suppa_arrow_box ctf_suppa_fa_box_top_arrow"><span aria-hidden="true" class="suppa-caret-down"></span></span>';

					}

					$item_output = '<div style="'.$this_item_position_css.' '.$display_item.'" class="suppa_menu suppa_menu_dropdown suppa_menu_'.$this->top_level_counter.'" ><a '.$attributes.' class="'.$class_names.' '.$has_arrow.'" >'.$link_html.$arrow.'</a>';
					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
				
				}	


				// Links
				else if( 'links' == $this->menu_type )
				{
					// options 
					$user_logged_in_out = get_post_meta( $item->ID, $this->menu_key.'link_user_logged', true);
					$display_item = ' display:none !important; ';
					if( $user_logged_in_out == 'both' )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_in' && is_user_logged_in() )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_out' && !is_user_logged_in() )
						{ $display_item = ''; }

					$this->links_column_width = get_post_meta( $item->ID, $this->menu_key.'links_column_width', true);
					$links_fullwidth = get_post_meta( $item->ID, $this->menu_key.'links_fullwidth', true);
					$container_style = 'style="width:100%; left:0px;"';
					$top_link_style = '';

					if( $links_fullwidth == "off" or $links_fullwidth == "" )
					{
						$container_width = get_post_meta( $item->ID, $this->menu_key.'links_width', true);
						$container_align = get_post_meta( $item->ID, $this->menu_key.'links_align', true);
						$container_style = 'style="width:'.$container_width.';';
						$top_link_style  = ' position:relative; ';

						if( $container_align == 'left' )
						{
							$container_style .= ' left:0px;" ';
						}
						else if( $container_align == 'right' )
						{
							$container_style .= ' right:0px;" ';
						}
						else 
						{
							$container_style .= ' left: 50%; margin-left: -'.( ( (int) $container_width ) / 2 ).'px; "';
						}						
					}

					$arrow = '';
					$has_arrow = '';
					if( in_array( 'menu-item-has-children' , $item->classes ) )
					{
						$has_arrow = ' suppa_top_links_has_arrow ';
						$arrow = '<span class="era_suppa_arrow_box ctf_suppa_fa_box_top_arrow"><span aria-hidden="true" class="suppa-caret-down"></span></span>';

					}

					$item_output = '<div style="'.$this_item_position_css.$top_link_style.$display_item.'" class="suppa_menu suppa_menu_links suppa_menu_'.$this->top_level_counter.'" ><a class="'.$class_names.' '.$has_arrow.'" '.$attributes.' >'.$link_html.$arrow.'</a>';
					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
					$output .= '<div class="suppa_submenu suppa_submenu_'.$this->top_level_counter.' suppa_submenu_columns_wrap" '.$container_style.' >';
				}


				// Posts
				else if( 'posts' == $this->menu_type )
				{
					$user_logged_in_out = get_post_meta( $item->ID, $this->menu_key.'link_user_logged', true);
					$display_item = ' display:none !important; ';
					if( $user_logged_in_out == 'both' )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_in' && is_user_logged_in() )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_out' && !is_user_logged_in() )
						{ $display_item = ''; }

					$output .= '<div style="'.$this_item_position_css.$display_item.'" class="suppa_menu suppa_menu_posts suppa_menu_'.$this->top_level_counter.'">';		

					// Reset the query
					wp_reset_query();

					// The Query : Load All Posts
					$args = array( 
									'suppress_filters'	=> true,
									'post_type' 		=> 'any',
									'posts_per_page' 	=> get_post_meta( $item->ID, $this->menu_key.'posts_number', true),
									'tax_query' 		=> array(
										array(
											'taxonomy' 	=> get_post_meta( $item->ID, $this->menu_key.'posts_taxonomy', true),
											'field' 	=> 'id',
											'terms' 	=> get_post_meta( $item->ID, $this->menu_key.'posts_category', true)
										)
									)								
								);

					$the_query = new WP_Query( $args );
					
					$posts_wrap = '';
					$posts_view_all = '';
					$has_arrow = ' suppa_top_links_has_arrow ';
					$upload_dir = wp_upload_dir();

					// The Loop
					if( $the_query->have_posts() ) :

					$output .= '<a class="'.$class_names.' '.$has_arrow.'" '.$attributes.' >'.$link_html.'<span class="era_suppa_arrow_box ctf_suppa_fa_box_top_arrow"><span aria-hidden="true" class="suppa-caret-down"></span></span></a>';
					$output .= '<div class="suppa_submenu suppa_submenu_posts" >';

					while ( $the_query->have_posts() ) :
						$the_query->the_post();

						$id 	= get_the_ID();
						//$thumb 	= wp_get_attachment_thumb_url( get_post_thumbnail_id( $id ) );
						$imgurl = wp_get_attachment_url( get_post_thumbnail_id( $id ) );
						$retina = $imgurl;

						$resized_width = (int)suppa_walkers::$offline_db['submenu-posts-post_width'];
						$resized_height = (int)suppa_walkers::$offline_db['submenu-posts-post_height'];

						// If Resize Enable 
						if( suppa_walkers::$project_settings['image_resize'] )
						{	
							$resized_img = preg_replace("/\.[a-zA-z]{1,4}$/", "", $imgurl);
							$resized_ext = preg_match("/\.[a-zA-z]{1,4}$/", $imgurl, $matches);

							$path = explode('uploads',$resized_img);

							$resized_path = "";
							$retina_path = "";
							
							if( isset($matches[0]) )
							{
								$tmp_resized = $resized_img;

								$resized_img = $tmp_resized.'-'.$resized_width.'x'.$resized_height.$matches[0];
								$retina_img  = $tmp_resized.'-'.$resized_width.'x'.$resized_height.'@2x'.$matches[0];
								
								$resized_path= $upload_dir['basedir'].$path[1].'-'.$resized_width.'x'.$resized_height.$matches[0];
								$retina_path = $upload_dir['basedir'].$path[1].'-'.$resized_width.'x'.$resized_height.'@2x'.$matches[0];
							}																

							// Detect if image exists
							if( file_exists($resized_path) )
							{
								$imgurl = $resized_img;
							}

							// Detect if retina image exists
							if( file_exists($retina_path) )
							{
								$retina = $retina_img;
							}

						}
						$post_title		= get_the_title();
						$post_link 		= get_permalink();
						$post_date 		= '<span class="suppa_post_link_date">'.get_the_date().'</span>';
						$post_comment_n	= '<span class="suppa_post_link_comments">'.get_comments_number().'</span>'; 
					
						$posts_wrap .= '<div class="suppa_post" >';
						$posts_wrap .= '<a href="'.$post_link.'" title="'.$post_title.'" >';
						$posts_wrap .= '<img style="width:'.$resized_width.'px;height:'.$resized_height.'px;" class="suppa_lazy_load '. suppa_walkers::$offline_db['posts_img_effect'] .'" data-original="'.$imgurl.'" data-retina="'.$retina.'" alt="'.$post_title.'" />';
						$posts_wrap .= '<div class="suppa_post_link_container" ><span class="suppa_post_link_title">'.$post_title.'</span></div>';
						$posts_wrap .= '</a><div class="suppa_clearfix"></div></div><!--suppa_post-->';

					endwhile;

					/* Restore original Post Data 
					 * NB: Because we are using new WP_Query we aren't stomping on the 
					 * original $wp_query and it does not need to be reset.
					*/
					wp_reset_postdata();
					
					if( suppa_walkers::$offline_db['latest_posts_view_all'] == 'on' )
					{
						$posts_view_all = '<a href="'.$item->url.'" class="suppa_latest_posts_view_all">'. __('View All...','suppa_menu') .'</a>';
					}

					$output .= $posts_wrap.$posts_view_all.'</div>';

					else:

					$output .= '<a class="'.$class_names.'" '.$attributes.' >'.$link_html.'</a>';

					endif;

					$output .= '</div>';				
				}	

				// mega_posts
				if( 'mega_posts' == $this->menu_type )
				{	
					$arrow = '';
					$has_arrow = '';
					if( in_array( 'menu-item-has-children' , $item->classes ) )
					{
						$has_arrow = ' suppa_top_links_has_arrow ';
						$arrow = '<span class="era_suppa_arrow_box ctf_suppa_fa_box_top_arrow"><span aria-hidden="true" class="suppa-caret-down"></span></span>';
					}

					$this->suppa_item_id = $item->ID;
					$this->mega_posts_items = array();

					$user_logged_in_out = get_post_meta( $item->ID, $this->menu_key.'link_user_logged', true);
					$display_item = ' display:none !important; ';
					if( $user_logged_in_out == 'both' )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_in' && is_user_logged_in() )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_out' && !is_user_logged_in() )
						{ $display_item = ''; }

					$item_output = '<div style="'.$this_item_position_css.$display_item.'" class="suppa_menu suppa_menu_mega_posts suppa_menu_'.$this->top_level_counter.'" ><a class="'.$class_names.' '.$has_arrow.'" '.$attributes.' >'.$link_html.$arrow.'</a>';
					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
					$output .= '<div class="suppa_submenu suppa_submenu_mega_posts" >';
				}

				// HTML & Shortcodes
				else if( 'html' == $this->menu_type )
				{
					$user_logged_in_out = get_post_meta( $item->ID, $this->menu_key.'link_user_logged', true);
					$display_item = ' display:none !important; ';
					if( $user_logged_in_out == 'both' )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_in' && is_user_logged_in() )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_out' && !is_user_logged_in() )
						{ $display_item = ''; }

					// options 
					$links_fullwidth = get_post_meta( $item->ID, $this->menu_key.'html_fullwidth', true);
					$container_style = 'style="width:100%; left:0px;"';
					$top_link_style = '';

					if( $links_fullwidth == "" )
					{
						$container_width = get_post_meta( $item->ID, $this->menu_key.'html_width', true);
						$container_align = get_post_meta( $item->ID, $this->menu_key.'html_align', true);
						$container_style = ' style="width:'.$container_width.'; ';
						$top_link_style  = ' position:relative; ';

						if( $container_align == 'left' )
						{
							$container_style .= ' left:0px;" ';
						}
						else if( $container_align == 'right' )
						{
							$container_style .= ' right:0px;" ';
						}
						else 
						{
							$container_style .= ' left: 50%; margin-left: -'.( ( (int) $container_width ) / 2 ).'px; "';
						}						
					}

					$content_and_shortcodes = get_post_meta( $item->ID, $this->menu_key.'html_content', true);
					$content_and_shortcodes = do_shortcode($content_and_shortcodes);
					
					$output .= '<div style="'.$this_item_position_css.$top_link_style.$display_item.'" class="suppa_menu suppa_menu_html suppa_menu_'.$this->top_level_counter.'" >';		

					$has_arrow = ' suppa_top_links_has_arrow ';
					if( $content_and_shortcodes != '' )
					{
						$output .= '<a class="'.$class_names.' '.$has_arrow.'" '.$attributes.' >'.$link_html.'<span class="era_suppa_arrow_box ctf_suppa_fa_box_top_arrow"><span aria-hidden="true" class="suppa-caret-down"></span></span></a>';
						$output .= '<div class="suppa_submenu suppa_submenu_html" '.$container_style.' >'.$content_and_shortcodes.'</div>';
					}
					else
					{
						$output .= '<a class="'.$class_names.'" '.$attributes.' >'.$link_html.'</a>';
					}

					$output .= '</div>';				
				}


				// Search Form
				else if( 'search' == $this->menu_type )
				{
					$user_logged_in_out = get_post_meta( $item->ID, $this->menu_key.'link_user_logged', true);
					$display_item = ' display:none !important; ';
					if( $user_logged_in_out == 'both' )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_in' && is_user_logged_in() )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_out' && !is_user_logged_in() )
						{ $display_item = ''; }

					$search_text = get_post_meta( $item->ID, $this->menu_key.'search_text', true);
					$output .= '<div style="'.$this_item_position_css.$display_item.'" class="suppa_menu suppa_menu_search suppa_menu_'.$this->top_level_counter.'">';	
					$output .= '<form action="'.get_bloginfo('url').'" method="get" class="suppa_search_form" >';	
					$output .= '<input type="text" name="s" class="suppa_search_input" value="" placeholder="'.$search_text.'" >';		
					$output .= '<button type="submit" class="suppa_search_button" >
									<span class="ctf_suppa_fa_box suppa_search_icon">
										<span aria-hidden="true" class="suppa-search"></span>
									</span>
								</button>

								<span class="ctf_suppa_fa_box suppa_search_icon_remove">
									<span aria-hidden="true" class="suppa-remove"></span>
								</span>

								';	
					$output .= '</form>';
					$output .= '</div>';	
				}

				// Social
				else if( 'social' == $this->menu_type )
				{
					$user_logged_in_out = get_post_meta( $item->ID, $this->menu_key.'link_user_logged', true);
					$display_item = ' display:none !important; ';
					if( $user_logged_in_out == 'both' )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_in' && is_user_logged_in() )
						{ $display_item = ''; }
					else if ( $user_logged_in_out == 'logged_out' && !is_user_logged_in() )
						{ $display_item = ''; }


					// Item Icon
					$link_html = '';
					if( $icon_type == "upload" )
					{
						$check_retina_icon = ( $icon_hover != "" ) ? $icon_hover : $icon;
						$link_html = '<img class="suppa_upload_img suppa_UP_icon_only" src="'.$icon.'" alt="'.$link_title.'" data-icon="'.$icon.'" data-retina="'.$check_retina_icon.'" >';
					}
					else if( $icon_type == "fontawesome" )
					{
						$link_html = '<span class="ctf_suppa_fa_box suppa_FA_icon_only"><span aria-hidden="true" class="'.$FA_icon.'" ></span></span>';
					}

					$output .= '<div style="'.$this_item_position_css.$display_item.'" class="suppa_menu suppa_menu_social suppa_menu_'.$this->top_level_counter.'">';		
					$output .= '<a class="'.$class_names.'" '.$attributes.' >'.$link_html.'</a>';
					$output .= '</div>';			
				}

			}


			// Dropdown 
			if( 'dropdown' == $this->menu_type )
			{
				if( $depth == 1 )
				{
					$this->dropdown_first_level_conuter += 1;
					$item_output = '<div class="suppa_dropdown_item_container"><a class="'.$class_names.'" '.$attributes.' >'.$link_html.'<span class="suppa_ar_arrow_right_'.$this->dropdown_first_level_conuter.'_'.$depth.'"></span></a> ';
					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
				}
				else if( $depth == 2 )
				{
					$this->dropdown_second_level_conuter += 1;
					$item_output = '<div class="suppa_dropdown_item_container"><a class="'.$class_names.'" '.$attributes.' >'.$link_html.'<span class="suppa_ar_arrow_right_'.$this->dropdown_second_level_conuter.'_'.$depth.'"></span></a> ';
					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
				}	
				else if( $depth == 3 )
				{
					$item_output = '<div class="suppa_dropdown_item_container"><a class="'.$class_names.'" '.$attributes.' >'.$link_html.'</span></a> ';
					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
				}
			}


			// Links
			if( 'links' == $this->menu_type )
			{
				if( $depth == 1 )
				{
					$output .= '<div class="suppa_column" style="width:'.$this->links_column_width.';">';
					$item_output = '<a class="'.$class_names.' suppa_column_title" '.$attributes.' >'.$link_html.'</a> ';
					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
				}		
				else if ( $depth >= 2 ) 
				{
					$item_output = '<a class="'.$class_names.' suppa_column_link" '.$attributes.' >'.$link_html.'</a> ';
					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
				}	
			}

			// Mega Posts
			if( 'mega_posts' == $this->menu_type )
			{
				if( $depth === 1 )
				{
					array_push( $this->mega_posts_items , $item->ID );

					$item_output = '<a class="'.$class_names.' suppa_mega_posts_link" '.$attributes.' data-cat="' . $item->ID . '" >'.$link_html.'<span class="era_suppa_arrow_box suppa_mega_posts_arrow"><span aria-hidden="true" class="suppa-caret-right"></span></span></a> ';
					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
				}
			}

			/*
			echo '<pre>';
			print_r( $item );
			echo '</pre><hr/>';
			*/

		}

		/**
		 * @see Walker::end_el()
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param object $item Page data object. Not used.
		 * @param int $depth Depth of page. Not Used.
		 */
		function end_el(&$output, $object, $depth = 0, $args = array() ) 
		{
			global $wp_query;

			// Dropdown 
			if( 'dropdown' == $this->menu_type )
			{
				$output .= '</div>';
			}
			// Links
			if( 'links' == $this->menu_type )
			{
				if( $depth === 0 )
				{
					$output .= '</div></div><!--suppa_submenu_columns_wrap-->';
				}
				if( $depth === 1 )
				{
					$output .= '</div>';
				}
			}

			// mega_posts
			if( 'mega_posts' == $this->menu_type )
			{
				if( $depth === 0 )
				{
					// Loop for mega posts need to be here
					$output .= '<div class="suppa_mega_posts_allposts">';
					foreach ( $this->mega_posts_items as $mega_posts_item ) 
					{

						$cat_id = get_post_meta( $mega_posts_item, $this->menu_key.'mega_posts_category', true);
						$cat_taxonomy = get_post_meta( $mega_posts_item, $this->menu_key.'mega_posts_taxonomy', true);
						
						$output .= '<div class="suppa_mega_posts_allposts_posts" data-cat="' . $mega_posts_item . '">';

						// Reset the query
						wp_reset_query();
						$mega_posts_number = get_post_meta( $this->suppa_item_id , '_menu-item-suppa-mega_posts_number' , true);
						
						// The Query : Load All Posts
						$args = array( 
										'suppress_filters'	=> true,
										'post_type' => 'any',
										'posts_per_page' 	=> $mega_posts_number /* get_post_meta( $item->ID, $this->menu_key.'posts_number', true) */ ,
										'tax_query' => array(
											array(
												'taxonomy' => $cat_taxonomy,
												'field' => 'id',
												'terms' => $cat_id
											)
										)								
									);

						$the_query = new WP_Query( $args );
						$posts_wrap = '';
						$upload_dir = wp_upload_dir();
						// The Loop
						while ( $the_query->have_posts() ) :
							$the_query->the_post();

							$id 	= get_the_ID();
							//$thumb 	= wp_get_attachment_thumb_url( get_post_thumbnail_id( $id ) );
							$imgurl = wp_get_attachment_url( get_post_thumbnail_id( $id ) );
							$retina = $imgurl;

							$resized_width = (int)suppa_walkers::$offline_db['submenu-megaposts-post_width'];
							$resized_height = (int)suppa_walkers::$offline_db['submenu-megaposts-post_height'];

							// If Resize Enable 
							if( suppa_walkers::$project_settings['image_resize'] )
							{	
								$resized_img = preg_replace("/\.[a-zA-z]{1,4}$/", "", $imgurl);
								$resized_ext = preg_match("/\.[a-zA-z]{1,4}$/", $imgurl, $matches);

								$path = explode('uploads',$resized_img);

								$resized_path = "";
								$retina_path = "";
								
								if( isset($matches[0]) )
								{
									$tmp_resized = $resized_img;

									$resized_img = $tmp_resized.'-'.$resized_width.'x'.$resized_height.$matches[0];
									$retina_img  = $tmp_resized.'-'.$resized_width.'x'.$resized_height.'@2x'.$matches[0];
									
									$resized_path= $upload_dir['basedir'].$path[1].'-'.$resized_width.'x'.$resized_height.$matches[0];
									$retina_path = $upload_dir['basedir'].$path[1].'-'.$resized_width.'x'.$resized_height.'@2x'.$matches[0];
								}																

								// Detect if image exists
								if( file_exists($resized_path) )
								{
									$imgurl = $resized_img;
								}

								// Detect if retina image exists
								if( file_exists($retina_path) )
								{
									$retina = $retina_img;
								}

							}
							$post_title		= get_the_title();
							$post_link 		= get_permalink();

							$posts_wrap .= '<a href="'.$post_link.'" title="'.$post_title.'" class="suppa_mega_posts_post_article" >';
							$posts_wrap .= '<img style="width:'.$resized_width.'px;height:'.$resized_height.'px;" class="suppa_lazy_load '. suppa_walkers::$offline_db['posts_img_effect'] .'" data-original="'.$imgurl.'" data-retina="'.$retina.'" alt="'.$post_title.'" />';
							$posts_wrap .= '<div class="suppa_post_link_container"><span>'.$post_title.'</span></div>';
							$posts_wrap .= '</a><!--suppa_mega_posts_post_article-->';

						endwhile;

						/* Restore original Post Data 
						 * NB: Because we are using new WP_Query we aren't stomping on the 
						 * original $wp_query and it does not need to be reset.
						*/
						wp_reset_postdata();

						$output .= $posts_wrap;

						$output .= '</div><!--suppa_mega_posts_allposts_posts-->';
				
					}
					$output .= '</div> <!--suppa_mega_posts_allposts-->';
				

					$output .= '</div></div><!--suppa_menu_mega_posts-->';
				}

			}

		}// End Func


	}// End Class
}


if( !class_exists( 'suppa_menu_backend_walker' ) )
{
/**
 * @package CTFramework
 * @since 1.0
 * @uses Walker_Nav_Menu
 */
	class suppa_menu_backend_walker extends Walker_Nav_Menu
	{
		protected $suppa_menu_type;
		protected $suppa_menu_father_and_sons_id = 0;

		/**
		 * @see Walker_Nav_Menu::start_lvl()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference.
		 * @param int $depth Depth of page.
		 */
		function start_lvl(&$output, $depth = 0, $args = array() ) {}

		/**
		 * @see Walker_Nav_Menu::end_lvl()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference.
		 * @param int $depth Depth of page.
		 */
		function end_lvl(&$output, $depth = 0, $args = array() ) {}

		/**
		 * @see Walker::start_el()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param object $item Menu item data object.
		 * @param int $depth Depth of menu item. Used for padding.
		 * @param int $current_page Menu item ID.
		 * @param object $args
		 */
		function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
			global $_wp_nav_menu_max_depth;
			$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			ob_start();
			$item_id = esc_attr( $item->ID );
			$removed_args = array(
				'action',
				'customlink-tab',
				'edit-menu-item',
				'menu-item',
				'page-tab',
				'_wpnonce',
			);

			$original_title = '';
			if ( 'taxonomy' == $item->type ) {
				$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
			} elseif ( 'post_type' == $item->type ) {
				$original_object = get_post( $item->object_id );
				$original_title = $original_object->post_title;
			}

			$classes = array(
				'menu-item menu-item-depth-' . $depth,
				'menu-item-' . esc_attr( $item->object ),
				'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
			);

			$title = $item->title;

			if ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
				$classes[] = 'pending';
				/* translators: %s: title of menu item in draft status */
				$title = sprintf( __('%s (Pending)','suppa_menu'), $item->title );
			}

			$title = empty( $item->label ) ? $title : $item->label;

			if( $depth == 0 )
			{
				$this->suppa_menu_father_and_sons_id = $this->suppa_menu_father_and_sons_id + 1;
				$this->suppa_menu_type = get_post_meta( $item->ID, '_menu-item-suppa-menu_type', true);
			}

			$depth_class = "suppa_menu_item suppa_menu_father_and_sons_id_" . $this->suppa_menu_father_and_sons_id . " suppa_menu_item_" . $this->suppa_menu_type . " ";

			?>

			<li  id="menu-item-<?php echo $item_id; ?>" class="<?php echo $depth_class; echo implode(' ', $classes ); ?>">

				<dl class="menu-item-bar">	
					
					<dt class="menu-item-handle" style="position:relative;" >
					
						<span class="item-title"><?php echo esc_html( $title ); ?></span>
						<span class="item-controls">

							<?php 
								$menu_type 	= get_post_meta( $item->ID, '_menu-item-suppa-menu_type', true); 
								if( $menu_type == "" )
								{
									$menu_type = 'dropdown';
								}

							?>
							<span class="item-type item-type-default"><?php echo "( ".esc_html( $menu_type )." )"; ?></span>

							<a class="item-edit" id="edit-<?php echo $item_id; ?>" title="<?php _e('Edit Menu Item','suppa_menu'); ?>" href="<?php
								echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) );
							?>"><?php _e( 'Edit Menu Item','suppa_menu' ); ?></a>
						</span>
					</dt>
				</dl>

				<div class="menu-item-settings" id="menu-item-settings-<?php echo $item_id; ?>">
					<?php if( 'custom' == $item->type ) : ?>
						<p class="field-url description description-wide">
							<label for="edit-menu-item-url-<?php echo $item_id; ?>">
								<?php _e( 'URL' ,'suppa_menu' ); ?><br />
								<input type="text" id="edit-menu-item-url-<?php echo $item_id; ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
							</label>
						</p>
					<?php endif; ?>
					<p class="description description-thin description-label avia_label_desc_on_active">
						<label for="edit-menu-item-title-<?php echo $item_id; ?>">
						<span class='avia_default_label'><?php _e( 'Navigation Label','suppa_menu' ); ?></span>
							<br />
							<input type="text" id="edit-menu-item-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
						</label>
					</p>
					<p class="description description-thin description-title">
						<label for="edit-menu-item-attr-title-<?php echo $item_id; ?>">
							<?php _e( 'Title Attribute','suppa_menu' ); ?><br />
							<input type="text" id="edit-menu-item-attr-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
						</label>
					</p>
					<p class="field-link-target description description-thin">
						<label for="edit-menu-item-target-<?php echo $item_id; ?>">
							<?php _e( 'link Target','suppa_menu' ); ?><br />
							<select id="edit-menu-item-target-<?php echo $item_id; ?>" class="widefat edit-menu-item-target" name="menu-item-target[<?php echo $item_id; ?>]">
								<option value="" <?php selected( $item->target, ''); ?>><?php _e('Same window or tab','suppa_menu'); ?></option>
								<option value="_blank" <?php selected( $item->target, '_blank'); ?>><?php _e('New window or tab','suppa_menu'); ?></option>
							</select>
						</label>
					</p>
					<p class="field-css-classes description description-thin">
						<label for="edit-menu-item-classes-<?php echo $item_id; ?>">
							<?php _e( 'CSS Classes (optional)','suppa_menu' ); ?><br />
							<input type="text" id="edit-menu-item-classes-<?php echo $item_id; ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo $item_id; ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
						</label>
					</p>
					<p class="field-xfn description description-thin">
						<label for="edit-menu-item-xfn-<?php echo $item_id; ?>">
							<?php _e( 'link Relationship (XFN)' ,'suppa_menu'); ?><br />
							<input type="text" id="edit-menu-item-xfn-<?php echo $item_id; ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
						</label>
					</p>
					<p class="field-description description description-wide">
						<label for="edit-menu-item-description-<?php echo $item_id; ?>">
							<?php _e( 'Description' ,'suppa_menu'); ?><br />
							<textarea id="edit-menu-item-description-<?php echo $item_id; ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo $item_id; ?>]"><?php echo esc_html( $item->post_content ); ?></textarea>
						</label>
					</p>

					<!-- *************** Suppa Options *************** -->
					<div class="admin_suppa_clearfix"></div>
					
					<br/>
					<br/>

					<div class='admin_suppa_options'>

						<!-- *************** new item *************** -->
						<div class="admin_suppa_box admin_suppa_box_menu_type" >
							
							<?php
								// Menu Type
								$title 	= __( 'Choose the menu type' , 'suppa_menu' );
								$key 	= "menu-item-suppa-menu_type";				

							?>

							<div class="admin_suppa_box_header">
								<span><?php echo $title; ?> :</span>
								<a>+</a>
							</div>


							<div class="admin_suppa_box_container">


								<!-- Select Menu Type -->

								<label for="edit-<?php echo $key.'-'.$item_id.'_1'; ?>">
									<input type="radio" value="dropdown" id="edit-<?php echo $key.'-'.$item_id; ?>" class="suppa_menu_type <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" <?php if($menu_type=="dropdown") echo "checked"; ?> /> &nbsp;&nbsp; <?php _e('Use as DropDown','suppa_menu'); ?><br/> 
								</label>
								<label for="edit-<?php echo $key.'-'.$item_id.'_3'; ?>">
									<input type="radio" value="links" id="edit-<?php echo $key.'-'.$item_id; ?>" class="suppa_menu_type <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" <?php if($menu_type=="links") echo "checked"; ?> /> &nbsp;&nbsp; <?php _e('Use as Mega Links','suppa_menu'); ?><br/> 
								</label>
								<label for="edit-<?php echo $key.'-'.$item_id.'_4'; ?>">
									<input type="radio" value="posts" id="edit-<?php echo $key.'-'.$item_id; ?>" class="suppa_menu_type <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" <?php if($menu_type=="posts") echo "checked"; ?> /> &nbsp;&nbsp; <?php _e('Use as Latest Posts','suppa_menu'); ?><br/>
								</label>

								<label for="edit-<?php echo $key.'-'.$item_id.'_4'; ?>">
									<input type="radio" value="mega_posts" id="edit-<?php echo $key.'-'.$item_id; ?>" class="suppa_menu_type <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" <?php if($menu_type=="mega_posts") echo "checked"; ?> /> &nbsp;&nbsp; <?php _e('Use as Mega Posts','suppa_menu'); ?><br/>
								</label>

								<label for="edit-<?php echo $key.'-'.$item_id.'_5'; ?>">
									<input type="radio" value="html" id="edit-<?php echo $key.'-'.$item_id; ?>" class="suppa_menu_type <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" <?php if($menu_type=="html") echo "checked"; ?> /> &nbsp;&nbsp; <?php _e('Use as HTML &amp; Shortcodes','suppa_menu'); ?><br/>
								</label>
								<label for="edit-<?php echo $key.'-'.$item_id.'_6'; ?>">
									<input type="radio" value="search" id="edit-<?php echo $key.'-'.$item_id; ?>" class="suppa_menu_type <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" <?php if($menu_type=="search") echo "checked"; ?> /> &nbsp;&nbsp; <?php _e('Use as Search Form','suppa_menu'); ?><br/>
								</label>
								<label for="edit-<?php echo $key.'-'.$item_id.'_7'; ?>">
									<input type="radio" value="social" id="edit-<?php echo $key.'-'.$item_id; ?>" class="suppa_menu_type <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" <?php if($menu_type=="social") echo "checked"; ?> /> &nbsp;&nbsp; <?php _e('Use as Social Media','suppa_menu'); ?><br/>
								</label>
								<br/><br/>


								<!-- Menu Type : Mega Posts -->
								<div <?php if( 'mega_posts' != $menu_type) echo "style='display:none;'"; ?> class="admin_suppa_box_option_inside admin_suppa_box_option_inside_mega_posts">
									
									<?php
										// Align
										$title 	= __( 'How Many Posts You Want To Show ?' , 'suppa_menu' );
										$key 	= "menu-item-suppa-mega_posts_number";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = 1;

										echo $title;
									?>

									<div>
										<select id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" >
										<?php
											for ( $i = 1; $i <= 20; $i++  ) 
											{
												if( $i == $value )
												{
													echo '<option selected="selected" >'.$i.'</option>';
												}
												else
												{
													echo '<option>'.$i.'</option>';
												}
											}
										?>	
										</select>
									</div>

									<div class="admin_suppa_clearfix"></div>
									
									<?php _e( 'Use Second Level Dropdown As a category' , 'suppa_menu' ); ?>
							
								</div>


								<!-- Menu Type : Social -->
								<div <?php if( 'social' != $menu_type) echo "style='display:none;'"; ?> class="admin_suppa_box_option_inside admin_suppa_box_option_inside_social">
								<?php _e( 'Try To Upload or select an icon from the "Link Settings" under this ' , 'suppa_menu' ); ?>
								</div>


								<!-- Menu Type : DropDown -->
								<div <?php if( 'dropdown' != $menu_type) echo "style='display:none;'"; ?> class="admin_suppa_box_option_inside admin_suppa_box_option_inside_dropdown">

									<?php
										// Width
										$title 	= __( 'Submenu Width' , 'suppa_menu' );
										$key 	= "menu-item-suppa-dropdown_width";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "180px";
									?>

									<span class="fl" ><?php echo $title; ?></span> 
									<input type="text" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class="fr <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" />

									<div class="admin_suppa_clearfix"></div>

								</div>



								<!-- Menu Type : Search -->
								<div <?php if( 'search' != $menu_type) echo "style='display:none;'"; ?> class="admin_suppa_box_option_inside admin_suppa_box_option_inside_search">

									<?php
										// Menu Type
										$title 	= __( 'Search Text' , 'suppa_menu' );
										$key 	= "menu-item-suppa-search_text";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "Search...";
									?>

									<span class="fl" ><?php echo $title; ?></span> 
									<input type="text" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class="fr <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" />
									
									<div class="admin_suppa_clearfix"></div>

								</div>


								<!-- Menu Type : Links -->
								<div <?php if( 'links' != $menu_type) echo "style='display:none;'"; ?> class="admin_suppa_box_option_inside admin_suppa_box_option_inside_links">

									<?php
										// FullWidth
										$title 	= __( 'FullWidth' , 'suppa_menu' );
										$key 	= "menu-item-suppa-links_fullwidth";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										$FullWidth_hide = '';
										$FullWidth 		= '';
										if( $value == "" ) 
										{
											$value 			= "off";
											$checked 		= "";
											$FullWidth 		= "off";
											$FullWidth_hide	= 'style="display:none;"';
										}
										else if ( $value == "off" )
										{
											$value 			= "off";
											$checked 		= "";
											$FullWidth 		= "off";
											$FullWidth_hide	= 'style="display:none;"';
										}
										else if( $value == "on" )
										{
											$value 			= "on";
											$checked 		= "checked";
											$FullWidth 		= "on";
											$FullWidth_hide	= "";
										}
									?>

									<span class="fl" ><?php echo $title; ?></span> 
									<div class="fr">
										<input <?php echo $checked; ?> type="checkbox" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class="admin_suppa_fullwidth_checkbox <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" />
									</div>

									<div class="admin_suppa_clearfix"></div>

									<br/>
									<?php
										// Container Width
										$title 	= __( 'Container Width' , 'suppa_menu' );
										$key 	= "menu-item-suppa-links_width";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") 
										{
											$value = "500px";
										}
									?>

									<span class="admin_suppa_fullwidth_div" <?php echo $FullWidth_hide; ?> ><?php echo $title; ?> </span> 
									<div class="admin_suppa_fullwidth_div" <?php echo $FullWidth_hide; ?> >
										<input type="text" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" />
									</div>

									<div class="admin_suppa_clearfix"></div>

									<br/>
									<?php
										// Align
										$title 	= __( 'Align' , 'suppa_menu' );
										$key 	= "menu-item-suppa-links_align";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "left";
									?>

									<span class="admin_suppa_fullwidth_div" <?php echo $FullWidth_hide; ?> ><?php echo $title; ?> </span> 
									<div class="admin_suppa_fullwidth_div" <?php echo $FullWidth_hide; ?> >
										<select id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" >
											<option <?php if( 'left' == $value ) echo 'selected="selected"'; ?> >left</option>
											<option <?php if( 'center' == $value ) echo 'selected="selected"'; ?> >center</option>
											<option <?php if( 'right' == $value ) echo 'selected="selected"'; ?> >right</option>
										</select>
									</div>

									<div class="admin_suppa_clearfix"></div>

									<br/>
									<?php
										// Container Width
										$title 	= __( 'Column Width' , 'suppa_menu' );
										$key 	= "menu-item-suppa-links_column_width";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") 
										{
											$value = "180px";
										}
									?>

									<span ><?php echo $title; ?> </span> 
									<div >
										<input type="text" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" />
									</div>

									<div class="admin_suppa_clearfix"></div>
									
								</div>



								<!-- Menu Type : Posts -->
								<div <?php if( 'posts' != $menu_type) echo "style='display:none;'"; ?> class="admin_suppa_box_option_inside admin_suppa_box_option_inside_posts">

									<?php
										// Select a Category
										$title 	= __( 'Select a Category' , 'suppa_menu' );
										$key 	= "menu-item-suppa-posts_category";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") 
										{
											$value = 0;
										}

										echo $title; 

									?>
									<div>
										<?php 
											$id 	= 'edit-'.$key.'-'.$item_id;
											$class 	= $key;
											$name 	= $key . "[". $item_id ."]";
											$selected_cat = $value;
											era_get_categories::all_cats_by_select( $id, $class, $name, $selected_cat ); 
										?>
									</div>

									<!-- Taxonomy -->
									<?php
										// Taxonomy
										$key 	= "menu-item-suppa-posts_taxonomy";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") 
										{
											$value = "category";
										}
									?>
									<input type="hidden" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class="suppa_taxonomy <?php echo $key; ?>" name="<?php echo $key . '['. $item_id .']';?>" />

									<div class="admin_suppa_clearfix"></div>

									<br/>
									<?php
										// Align
										$title 	= __( 'Posts Number' , 'suppa_menu' );
										$key 	= "menu-item-suppa-posts_number";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = 1;

										echo $title;
									?>

									<div>
										<select id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" >
										<?php
											for ( $i = 1; $i <= 20; $i++  ) 
											{
												if( $i == $value )
												{
													echo '<option selected="selected" >'.$i.'</option>';
												}
												else
												{
													echo '<option>'.$i.'</option>';
												}
											}
										?>	
										</select>
									</div>

									<div class="admin_suppa_clearfix"></div>

								</div>	

								<!-- Menu Type : HTML -->
								<div <?php if( 'html' != $menu_type) echo "style='display:none;'"; ?> class="admin_suppa_box_option_inside admin_suppa_box_option_inside_html">

									<?php _e( 'WordPress Editor' , 'suppa_menu'); ?>
									<div>
										<button id="<?php echo $item_id; ?>" class="admin_suppa_edit_button button-primary">Edit</button> <br/> 
									</div>

									<div class="admin_suppa_clearfix"></div>

									<?php
										// Editor Content
										$key 	= "menu-item-suppa-html_content";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "";
									?>

									<textarea id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" ><?php echo $value; ?></textarea>

									<br/>
									<br/>

									<?php
										// FullWidth
										$title 	= __( 'FullWidth' , 'suppa_menu' );
										$key 	= "menu-item-suppa-html_fullwidth";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										$FullWidth_hide = '';
										$FullWidth 		= '';
										if($value != "") 
										{
											$value 			= "checked";
											$FullWidth 		= "on";
											$FullWidth_hide	= 'style="display:none;"';
										}
									?>

									<?php echo $title; ?> 
									<div>
										<input <?php echo $value; ?> type="checkbox" value="on" id="edit-<?php echo $key.'-'.$item_id; ?>" class="admin_suppa_fullwidth_checkbox <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" />
									</div>

									<div class="admin_suppa_clearfix"></div>

									<br/>
									<?php
										// Container Width
										$title 	= __( 'Container Width' , 'suppa_menu' );
										$key 	= "menu-item-suppa-html_width";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") 
										{
											$value = "500px";
										}
									?>

									<span class="admin_suppa_fullwidth_div" <?php echo $FullWidth_hide; ?> ><?php echo $title; ?> </span> 
									<div class="admin_suppa_fullwidth_div" <?php echo $FullWidth_hide; ?> >
										<input type="text" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" />
									</div>

									<div class="admin_suppa_clearfix"></div>

									<br/>
									<?php
										// Align
										$title 	= __( 'Align' , 'suppa_menu' );
										$key 	= "menu-item-suppa-html_align";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "left";
									?>

									<span class="admin_suppa_fullwidth_div" <?php echo $FullWidth_hide; ?> ><?php echo $title; ?> </span> 
									<div class="admin_suppa_fullwidth_div" <?php echo $FullWidth_hide; ?> >
										<select id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" >
											<option <?php if( 'left' == $value ) echo 'selected="selected"'; ?> >left</option>
											<option <?php if( 'center' == $value ) echo 'selected="selected"'; ?> >center</option>
											<option <?php if( 'right' == $value ) echo 'selected="selected"'; ?> >right</option>
										</select>
									</div>

									<div class="admin_suppa_clearfix"></div>

								</div>	

							</div>
						</div>
						<!-- *************** end item *************** -->

						<!-- *************** new item *************** -->
						<div class="admin_suppa_clearfix"></div>
						<div class="admin_suppa_box admin_suppa_box_mega_posts" >

							<div class="admin_suppa_box_header">
								<span><?php _e('Mega Menu Child Category','suppa_menu'); ?></span>
								<a>+</a>
							</div>
							<div class="admin_suppa_box_container admin_suppa_box_container_mega_posts">

								<div id="menu-item-suppa-mega_posts" >

									<?php
										// Select a Category
										$title 	= __( 'Select a Category' , 'suppa_menu' );
										$key 	= "menu-item-suppa-mega_posts_category";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") 
										{
											$value = 0;
										}

										echo $title; 

									?>
									<div>
										<?php 
											$id 	= 'edit-'.$key.'-'.$item_id;
											$class 	= $key;
											$name 	= $key . "[". $item_id ."]";
											$selected_cat = $value;
											era_get_categories::all_cats_by_select( $id, $class, $name, $selected_cat ); 
										?>
									</div>

									<!-- Taxonomy -->
									<?php
										// Taxonomy
										$key 	= "menu-item-suppa-mega_posts_taxonomy";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") 
										{
											$value = "category";
										}
									?>
									<input type="hidden" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class="suppa_taxonomy <?php echo $key; ?>" name="<?php echo $key . '['. $item_id .']';?>" />

									<div class="admin_suppa_clearfix"></div>

								</div>

							</div>
						</div>
						<!-- *************** end item *************** -->

						<!-- *************** new item *************** -->
						<div class="admin_suppa_clearfix"></div>
						<div class="admin_suppa_box admin_suppa_box_link_settings" >

							<div class="admin_suppa_box_header">
								<span><?php _e('Link Settings :','suppa_menu'); ?></span>
								<a>+</a>
							</div>
							<div class="admin_suppa_box_container admin_suppa_box_container_settings">

								<div id="menu-item-suppa-link_logged_in" >
									<?php
										// User Logged In / Out
										$title 	= __( 'Show link when user' , 'suppa_menu' );
										$key 	= "menu-item-suppa-link_user_logged";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "both";
									?>

									<?php echo $title; ?> 
									<select id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" >
										<option value="both" <?php if( 'both' == $value ) echo 'selected="selected"'; ?> ><?php _e('Both Logged in/out','suppa_menu'); ?></option>
										<option value="logged_in" <?php if( 'logged_in' == $value ) echo 'selected="selected"'; ?> ><?php _e('Logged in','suppa_menu'); ?></option>
										<option value="logged_out" <?php if( 'logged_out' == $value ) echo 'selected="selected"'; ?> ><?php _e('Logged out','suppa_menu'); ?></option>
									</select>

									<div class="admin_suppa_clearfix"></div>

								</div>

								<br/>

								<div id="menu-item-suppa-link_position_container" >
									<?php
										// Position
										$title 	= __( 'Position' , 'suppa_menu' );
										$key 	= "menu-item-suppa-link_position";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "left";
									?>

									<?php echo $title; ?> 
									<select id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" >
										<option value="left" <?php if( 'left' == $value ) echo 'selected="selected"'; ?> ><?php _e('Left','suppa_menu'); ?></option>
										<option value="right" <?php if( 'right' == $value ) echo 'selected="selected"'; ?> ><?php _e('Right','suppa_menu'); ?></option>
										<option value="none" <?php if( 'none' == $value ) echo 'selected="selected"'; ?> ><?php _e('None','suppa_menu'); ?></option>
									</select>

									<div class="admin_suppa_clearfix"></div>

								</div>
							

								
								<?php
									// Upload or Font Awesome
									$title 	= __( 'Upload Icon' , 'suppa_menu' );
									$key 	= "menu-item-suppa-link_icon_type";
									$value 	= get_post_meta( $item->ID, '_'.$key, true);

									if($value == "")
									{ $value = "icon_type"; }

									$icon_type = $value;
								?>
								<label for="edit-<?php echo $key.'-'.$item_id; ?>" >
									
									<?php echo $title; ?>

									<select id="edit-<?php echo $key.'-'.$item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>"  >
										<option value="icon_type" <?php if( 'icon_type' == $value ) echo 'selected="selected"' ?> ><?php _e('Icon Type','suppa_menu'); ?></option>										
										<option value="upload" <?php if( 'upload' == $value ) echo 'selected="selected"' ?> ><?php _e('Upload new icon','suppa_menu'); ?></option>
										<option value="fontawesome" <?php if( 'fontawesome' == $value ) echo 'selected="selected"' ?> ><?php _e('Font Awesome icon','suppa_menu'); ?></option>
									</select>

									<div class="admin_suppa_clearfix"></div>

								</label>

								<div <?php if( $icon_type != 'upload' ) echo 'style="display:none;"' ; ?> class="admin_suppa_box_option_inside admin_suppa_box_option_inside_icon_upload">

									<?php
										// Use icon only
										$title 	= __( 'Icon' , 'suppa_menu' );
										$key 	= "menu-item-suppa-link_icon_image";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "";
										$uploaded_icon = $value;
									?>
									<?php echo $title; ?>
									<div>
										<input type="text" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class="admin_suppa_upload_input <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" /> 
										
										<button class="admin_suppa_upload button-primary fr">Upload</button> <br/> 
									</div>

									<div class="admin_suppa_clearfix"></div>

									<br/>
									<?php
										// Use icon only
										$title 	= __( 'Icon (Retina)' , 'suppa_menu' );
										$key 	= "menu-item-suppa-link_icon_image_hover";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "";
									?>
									<?php echo $title; ?>
									<div>
										<input type="text" value="<?php echo $value; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class="admin_suppa_upload_input <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" /> 
										
										<button class="admin_suppa_upload button-primary fr">Upload</button> <br/> 
									</div>
									<div class="admin_suppa_clearfix"></div>

								</div>

								<div <?php if( $icon_type != 'fontawesome' ) echo 'style="display:none;"' ; ?> class="admin_suppa_box_option_inside admin_suppa_box_option_inside_icon_fontawesome">

									<?php
										// Use icon only
										$title 	= __( 'Icon' , 'suppa_menu' );
										$key 	= "menu-item-suppa-link_icon_fontawesome";
										$value 	= get_post_meta( $item->ID, '_'.$key, true);

										if($value == "") $value = "";
										$value_icon = $value;
									?>
									<?php echo $title; ?>

									<button id="<?php echo $item->ID; ?>" class="admin_suppa_selectIcon_button button-primary fr">Select</button> 
									<input type="hidden" value="<?php echo $value_icon; ?>" id="edit-<?php echo $key.'-'.$item_id; ?>" class="fr admin_suppa_fontAwesome_icon_hidden-<?php echo $item->ID; ?> <?php echo $key; ?>" name="<?php echo $key . "[". $item_id ."]";?>" /> 
									<div class="admin_suppa_clearfix"></div>

									<br/>

									<span class="admin_suppa_fontAwesome_icon_box_preview admin_suppa_fontAwesome_icon_box_preview-<?php echo $item->ID; ?>">
										<span style="font-size:20px; ?>;" aria-hidden="true" class="<?php echo $value_icon; ?>"></span>
									</span>

									<br/><br/>

								</div>

							</div>
						</div>
						<!-- *************** end item *************** -->

					</div>
					<!-- *************** end Suppa Options *************** -->

					<div class="menu-item-actions description-wide submitbox">
						<?php if( 'custom' != $item->type ) : ?>
							<p class="link-to-original">
								<?php printf( __('Original: %s','suppa_menu'), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
							</p>
						<?php endif; ?>
						<a class="item-delete submitdelete deletion" id="delete-<?php echo $item_id; ?>" href="<?php
						echo wp_nonce_url(
							add_query_arg(
								array(
									'action' => 'delete-menu-item',
									'menu-item' => $item_id,
								),
								remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
							),
							'delete-menu_item_' . $item_id
						); ?>"><?php _e('Remove','suppa_menu'); ?></a> <span class="meta-sep"> | </span> <a class="item-cancel submitcancel" id="cancel-<?php echo $item_id; ?>" href="<?php	echo add_query_arg( array('edit-menu-item' => $item_id, 'cancel' => time()), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) ) );
							?>#menu-item-settings-<?php echo $item_id; ?>">Cancel</a>
					</div>

					<input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo $item_id; ?>]" value="<?php echo $item_id; ?>" />
					<input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
					<input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
					<input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
					<input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
					<input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
				</div><!-- .menu-item-settings-->
				<ul class="menu-item-transport"></ul>
			<?php
			$output .= ob_get_clean();
		}
	}


}