<?php

if( !class_exists('suppa_tour') )
{
	class suppa_tour
	{

		function __construct()
		{
			add_action( 'admin_enqueue_scripts' , array( $this , 'suppa_tour_assets' ) );
			add_action( 'admin_print_footer_scripts' , array( $this , 'suppa_steps' ) );
		}


		function suppa_tour_assets()
		{
			wp_enqueue_style( 'wp-pointer' );
			wp_enqueue_script( 'wp-pointer' );
		}

		function suppa_steps()
		{
			$site_url = get_admin_url();

			$step = 0;
			if( !get_option('suppa_tour') )
			{
				update_option('suppa_tour',0);
			}
			else
			{
				$step = get_option('suppa_tour');
			}

			$all_steps = array(

				0 => array (
						'target'	=> '#suppa-menu',
						'class' 	=> 'suppa_tour',
						'title'		=> 'Hello &amp; Welcome to Suppa Menu',
						'content'	=> 'Please click on "Next" to start building your menu.',
						'buttons'	=> '<a href="'.$site_url."nav-menus.php?suppa_tour_step=0".'" class="suppa_step_0_button button button-large button-primary">Next</a>',
						'edge'		=> 'top',
						'align'		=> 'left'
					),

				1 => array (
						'target'	=> '#suppa-menu',
						'class' 	=> 'suppa_tour',
						'title'		=> 'Hello &amp; Welcome to Suppa Menu',
						'content'	=> 'Please click on "Next" to start building your menu.',
						'buttons'	=> '<a href="'.$site_url."nav-menus.php?suppa_tour_step=0".'" class="suppa_step_1_button button button-large button-primary">Next</a>',
						'edge'		=> 'top',
						'align'		=> 'left'
					),

				);


			echo "
			<script type='text/javascript'>
				jQuery(document).ready( function() {
				        jQuery('".$all_steps[$step]['target']."').pointer(
							{
							    	pointerClass: '".$all_steps[$step]['class']."',
							        content: '<h3>".$all_steps[$step]['title']."</h3><p>".$all_steps[$step]['content']."</p><p>".$all_steps[$step]['buttons']."</p>',
							        position: {
							            edge: '".$all_steps[$step]['edge']."',
							            align: '".$all_steps[$step]['align']."'
							        }
							}
				         ).pointer('open');
				});
			</script>
			";



		}
	}

}