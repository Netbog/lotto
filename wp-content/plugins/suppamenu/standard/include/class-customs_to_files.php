<?php
/**
 * Save Custom Css & JS to Files
 *
 * @package 	CTFramework
 * @author		Sabri Taieb ( codezag )
 * @copyright	Copyright (c) Sabri Taieb
 * @link		http://codetemp.com
 * @since		Version 1.5
 *
 */


if( !class_exists('suppa_customs2files') )
{
	/**
	* 
	*/
	class suppa_customs2files
	{
		protected $project_settings;

		function __construct( $project_settings )
		{
			$this->project_settings = $project_settings;

			/** Project Actions / Filters **/
			add_action( $this->project_settings['plugin_id'] . '_after_db_save' , array( $this, 'save_to_files' ), 10, 2);
			add_action( $this->project_settings['plugin_id'] . '_after_db_reset' , array( $this, 'delete_files' ), 10, 2);
		}

		function delete_files()
		{
			$all_files = array(
				'suppa_ace_custom.css',
				'suppa_js_settings.js',
				'suppa_user_style.css',
				'suppa_css_settings.js',
			);
			$upload_dir = wp_upload_dir();
			if( is_writable($upload_dir['basedir']) )
			{
				foreach ($all_files as $file ) 
				{
					if( file_exists( $upload_dir['basedir'] .'/'. $file ) )
						@unlink( $upload_dir['basedir'] .'/'. $file );
				}
			}
		}

		function save_to_files( $settings_array )
		{					
			$style = $settings_array;


			/** Uploads Folder Check **/
			$upload_dir = wp_upload_dir();
			if( !is_writable($upload_dir['basedir']) )
			{
				update_option('suppa_js_settings_file' , 'none' );
				update_option('suppa_custom_style_file' , 'none' );
				update_option('suppa_current_skin_json' , 'none' );

				die( __('Uploads Folder Must Be Writable','suppa_menu') );
			}
			else
			{
				wp_mkdir_p( $upload_dir['basedir']. '/suppamenu' );
			}

			/** Create Json Files **/
			$json = $settings_array;
			unset( $json[''] );

			
			$my_file = $upload_dir['basedir'].'/suppamenu/suppa_current_skin.json';
			$handle = @fopen($my_file, 'w');
			fwrite($handle, json_encode($json) );
			fclose($handle);

			/** Save Generated File Link To Database **/
			update_option('suppa_current_skin_json' , $upload_dir['baseurl'].'/suppamenu/suppa_current_skin.json' );


			/** Background Images **/
			// Menu Background Image 
			$menu_background = '';
			if( isset( $style['menu_bg_bg_image'] ) and $style['menu_bg_bg_image'] != '' )
			{
				$menu_background 	= 'background-image:url(\''. $style['menu_bg_bg_image'].'\') !important; 
									    background-repeat:'.$style['menu_bg_bg_repeat'].'; 
										background-attachment:'.$style['menu_bg_bg_attachment'].'; 
										background-position:'.$style['menu_bg_bg_position'].';  
										';
			}
			// SubMenu Background Image 
			$submenu_background_image = '';
			if( isset( $style['submenu-bg_bg_image'] ) and $style['submenu-bg_bg_image'] != '' )
			{
				$submenu_background_image = 'background-image:url(\''. $style['submenu-bg_bg_image'].'\') !important; 
											 background-repeat:'.$style['submenu-bg_bg_repeat'].'; 
											 background-attachment:'.$style['submenu-bg_bg_attachment'].'; 
											 background-position:'.$style['submenu-bg_bg_position'].';  
											';
			}
			// SubMenu Background Image 
			$rwd_background_image = '';
			if( isset( $style['rwd_container_bg_bg_image'] ) and $style['rwd_container_bg_bg_image'] != '' )
			{
				$rwd_background_image = 'background-image:url(\''. $style['rwd_container_bg_bg_image'].'\') !important; 
											 background-repeat:'.$style['rwd_container_bg_bg_repeat'].'; 
											 background-attachment:'.$style['rwd_container_bg_bg_attachment'].'; 
											 background-position:'.$style['rwd_container_bg_bg_position'].';  
											';
			}

			/** The CSS Generator **/
			$custom_css = 
			'
				/** ----------------------------------------------------------------
				 ******** General Style
				 ---------------------------------------------------------------- **/	

				.suppa_holder{
					height:'.$style['menu_height'].' !important;
				}

				.suppaMenu_wrap {
					height:'.$style['menu_height'].' !important;
					z-index:'.$style['menu_z_index'].';
				}

				.suppaMenu_wrap_wide_layout {
					background-color:'.$style['menu_bg_bg_color'].';
					
					/* Borders */
					border-top: '.$style['menu_border_top_size'].' solid '.$style['menu_border_top_color'].';
					border-left: '.$style['menu_border_left_size'].' solid '.$style['menu_border_left_color'].';

					/* CSS3 Gradient */ 
					background-image: -webkit-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: -moz-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: -o-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: -ms-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;	
				
					'.$menu_background.'
				}

				.suppaMenu {

					width:'.$style['menu_width'].';
					z-index:'.$style['menu_z_index'].';
					height:'.$style['menu_height'].' !important;

					background-color:'.$style['menu_bg_bg_color'].';
					
					/* Borders */
					border-top: '.$style['menu_border_top_size'].' solid '.$style['menu_border_top_color'].';
					border-right: '.$style['menu_border_right_size'].' solid '.$style['menu_border_right_color'].';
					border-bottom: '.$style['menu_border_bottom_size'].' solid '.$style['menu_border_bottom_color'].';
					border-left: '.$style['menu_border_left_size'].' solid '.$style['menu_border_left_color'].';

					/* CSS3 Gradient */ 
					background-image: -webkit-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: -moz-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: -o-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: -ms-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;

					'.$menu_background.'

					/* CSS3 Box Shadow */
					-moz-box-shadow   : 0px 0px '.$style['menu_boxshadow_blur'].' '.$style['menu_boxshadow_distance'].' '.$style['menu_boxshadow_color'].';
					-webkit-box-shadow: 0px 0px '.$style['menu_boxshadow_blur'].' '.$style['menu_boxshadow_distance'].' '.$style['menu_boxshadow_color'].';
					box-shadow        : 0px 0px '.$style['menu_boxshadow_blur'].' '.$style['menu_boxshadow_distance'].' '.$style['menu_boxshadow_color'].';
				
					/* CSS3 Border Radius */
					-webkit-border-radius: '.$style['menu_borderradius_top_left'].' '.$style['menu_borderradius_top_right'].' '.$style['menu_borderradius_bottom_right'].' '.$style['menu_borderradius_bottom_left'].'; 
					-moz-border-radius: '.$style['menu_borderradius_top_left'].' '.$style['menu_borderradius_top_right'].' '.$style['menu_borderradius_bottom_right'].' '.$style['menu_borderradius_bottom_left'].'; 
					border-radius: '.$style['menu_borderradius_top_left'].' '.$style['menu_borderradius_top_right'].' '.$style['menu_borderradius_bottom_right'].' '.$style['menu_borderradius_bottom_left'].'; 
				}


				/** ----------------------------------------------------------------
				 ******** Logo Style
				 ---------------------------------------------------------------- **/	
				.suppa_menu_logo img{
					padding : '.$style['title-padding_top'].' '.$style['title-padding_right'].' 0px '.$style['title-padding_left'].' !important;
				}
				.suppa_menu_logo {
					height:'.$style['menu_height'].';
					border-right:1px solid '.$style['top-links-border_color'].' !important;
				}




				/** ----------------------------------------------------------------
				 ******** Top Level Links Style
				 ---------------------------------------------------------------- **/	
				.suppa_menu_mega_posts .suppa_top_level_link ,
				.suppa_menu_dropdown .suppa_top_level_link ,
				.suppa_menu_posts .suppa_top_level_link ,
				.suppa_menu_html .suppa_top_level_link ,
				.suppa_menu_links .suppa_top_level_link {
					font-size:'.$style['top_level_font_font_size'].'px !important;
					font-family:'.$style['top_level_font_font_family'].' !important;
					'.$style['top_level_font_font_family_style'].'				
				}

				.suppa_top_level_link {
					line-height:'.$style['menu_height'].' !important;
					height:'.$style['menu_height'].' !important;
					color:'.$style['top_level_font_font_color'].';
				}

				.suppa_top_level_link .suppa_item_title {
					font-size:'.$style['top_level_font_font_size'].'px !important;
					font-family:'.$style['top_level_font_font_family'].' !important;
					'.$style['top_level_font_font_family_style'].'
					color:'.$style['top_level_font_font_color'].';	
				}

				.suppa_menu {
					height:'.$style['menu_height'].' !important;
				}

				.suppa_menu_mega_posts .suppa_top_level_link ,
				.suppa_menu_dropdown .suppa_top_level_link ,
				.suppa_menu_posts .suppa_top_level_link ,
				.suppa_menu_html .suppa_top_level_link ,
				.suppa_menu_links .suppa_top_level_link {
					padding-left:'.$style['top_level_padding_left'].';
					padding-right:'.$style['top_level_padding_left'].';
					border-color:'.$style['top-links-border_color'].' !important;
				}

				.suppa_menu .suppa_top_level_link.suppa_top_links_has_arrow{
					padding-right:'.$style['top_level_padding_right'].';
				}

				/** ----------------------------------------------------------------
				 ******** Top Level Links on [HOVER] Style
				 ---------------------------------------------------------------- **/	
				.suppa_menu:hover .suppa_top_level_link {
					background-color:'.$style['top_level_bg_hover'].';
					color:'.$style['top_level_links_color_hover'].';
				}
				.suppa_menu:hover .suppa_top_level_link .suppa_item_title {
					color:'.$style['top_level_links_color_hover'].';
				}

				/** Needed for suppa_frontend.js **/
				.suppa_menu.suppa_menu_class_hover .suppa_top_level_link {
					background-color:'.$style['top_level_bg_hover'].' !important;
					color:'.$style['top_level_links_color_hover'].' !important;
				}
				.suppa_menu.suppa_menu_class_hover .suppa_top_level_link .suppa_item_title {
					color:'.$style['top_level_links_color_hover'].' !important;
				}

				/* boder right or left */
				.suppa_menu .suppa_top_level_link.suppa_menu_position_left {
					border-right:1px solid '.$style['top-links-border_color'].';
				}
				.suppa_menu .suppa_top_level_link.suppa_menu_position_right {
					border-left:1px solid '.$style['top-links-border_color'].';
				}

				/** ----------------------------------------------------------------
				 ******** Top Level Arrow Style
				 ---------------------------------------------------------------- **/				
				.suppa_top_level_link .ctf_suppa_fa_box_top_arrow{
						font-size:'.$style['top-links-arrow_width'].' !important;
						top:'.$style['top-links-arrow_position_top'].' !important;
						right:'.$style['top-links-arrow_position_right'].' !important;
						
						/* color/bg/border */
						color:'.$style['top-links-arrow_color'].';
				}
				.suppa_menu:hover .suppa_top_level_link .ctf_suppa_fa_box_top_arrow{
					color:'.$style['top-links-arrow_color_hover'].' !important;
				}
				/** Needed for suppa_frontend.js **/
				.suppa_menu.suppa_menu_class_hover .suppa_top_level_link .ctf_suppa_fa_box_top_arrow,
				.suppa_menu.suppa_menu_class_hover .suppa_top_level_link .ctf_suppa_fa_box{
					color:'.$style['top-links-arrow_color_hover'].' !important;
				}

				/** ----------------------------------------------------------------
				 ******** Current Top Level Style
				 ---------------------------------------------------------------- **/				
				.suppa_menu .suppa_top_level_link.current-menu-item,
				.suppa_menu .suppa_top_level_link.current-menu-item .ctf_suppa_fa_box,
				.suppa_menu .suppa_top_level_link.current-menu-item .suppa_item_title,
				.suppa_menu .suppa_top_level_link.current-menu-ancestor,
				.suppa_menu .suppa_top_level_link.current-menu-ancestor .ctf_suppa_fa_box,
				.suppa_menu .suppa_top_level_link.current-menu-ancestor .suppa_item_title{
					color:'.$style['top-links-current_color'].' ;
				}

				.suppa_menu .suppa_top_level_link.current-menu-item,
				.suppa_menu .suppa_top_level_link.current-menu-ancestor {
					background-color:'.$style['top-links-current_bg'].';
				}

				.suppa_menu .suppa_top_level_link.current-menu-ancestor .era_suppa_arrow_box span,
				.suppa_menu .suppa_top_level_link.current-menu-item .era_suppa_arrow_box span{
					color:'.$style['top-links-current_arrow_color'].';
				}

				/** ----------------------------------------------------------------
				 ******** Top Level Icons
				 ---------------------------------------------------------------- **/
				/** F.Awesome Icons **/
				.suppa_menu .suppa_top_level_link .ctf_suppa_fa_box{
					color:'.$style['top_level_font_font_color'].';
				}
				.suppa_menu:hover .suppa_top_level_link .ctf_suppa_fa_box{
					color:'.$style['top_level_links_color_hover'].';
				}
				.suppa_menu .suppa_top_level_link .ctf_suppa_fa_box{
					font-size:'.$style['fontawesome_icons_size'].' !important;
					margin-top: '.$style['top-links-fontawesome_icon_margin_top'].' !important;
					padding-right: '.$style['top-links-fontawesome_icon_margin_right'].' !important;
				}

				/** Uploaded Icons **/
				.suppa_menu .suppa_top_level_link .suppa_upload_img{
					width : '.$style['uploaded_icons_width'].' !important; 
					height : '.$style['uploaded_icons_width'].' !important; 
					margin-top: '.$style['top-links-normal_icon_margin_top'].' !important;
					padding-right: '.$style['top-links-normal_icon_margin_right'].' !important;
				}



				/** ----------------------------------------------------------------
				 ******** General Submenu
				 ---------------------------------------------------------------- **/
				.suppa_submenu {
					
					top:'.( (int)$style['menu_height'] + (int)$style['menu_border_bottom_size'] ).'px !important;

					/* color/bg/border */
					background-color:'.$style['submenu-bg_bg_color'].';
									
					border-top: '.$style['submenu-border-top_size'].' solid '.$style['submenu-border-top_color'].';
					border-right: '.$style['submenu-border-right_size'].' solid '.$style['submenu-border-right_color'].';
					border-bottom: '.$style['submenu-border-bottom_size'].' solid '.$style['submenu-border-bottom_color'].';
					border-left: '.$style['submenu-border-left_size'].' solid '.$style['submenu-border-left_color'].';

					/* CSS3 Gradient */ 
					background-image: -webkit-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: -moz-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: -o-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: -ms-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;

					'.$submenu_background_image.'

					/* CSS3 Box Shadow */
					-moz-box-shadow   : 0px 0px '.$style['submenu-boxshadow_blur'].' '.$style['submenu-boxshadow_distance'].' '.$style['submenu-boxshadow_color'].';
					-webkit-box-shadow: 0px 0px '.$style['submenu-boxshadow_blur'].' '.$style['submenu-boxshadow_distance'].' '.$style['submenu-boxshadow_color'].';
					box-shadow        : 0px 0px '.$style['submenu-boxshadow_blur'].' '.$style['submenu-boxshadow_distance'].' '.$style['submenu-boxshadow_color'].';
				
					/* CSS3 Border Radius */
					-webkit-border-radius: '.$style['submenu-borderradius_top_left'].' '.$style['submenu-borderradius_top_right'].' '.$style['submenu-borderradius_bottom_right'].' '.$style['submenu-borderradius_bottom_left'].'; 
					-moz-border-radius: '.$style['submenu-borderradius_top_left'].' '.$style['submenu-borderradius_top_right'].' '.$style['submenu-borderradius_bottom_right'].' '.$style['submenu-borderradius_bottom_left'].'; 
					border-radius: '.$style['submenu-borderradius_top_left'].' '.$style['submenu-borderradius_top_right'].' '.$style['submenu-borderradius_bottom_right'].' '.$style['submenu-borderradius_bottom_left'].'; 			
				}



				/** ----------------------------------------------------------------
				 ******** SubMenu Posts 
				 ---------------------------------------------------------------- **/
				.suppa_post {
					width: '.$style['submenu-posts-post_width'].';
				}
					.suppa_post a{
						width: '.$style['submenu-posts-post_width'].';
					}
					.suppa_post img{
						width: '.$style['submenu-posts-post_width'].';
						height: '.$style['submenu-posts-post_height'].';
					}
					.suppa_post div.suppa_post_link_container {
						width: '.$style['submenu-posts-post_width'].' !important;
					}

				.suppa_post {
					margin: '.$style['submenu-posts-post_margin_top'].' '.$style['submenu-posts-post_margin_right'].' '.$style['submenu-posts-post_margin_bottom'].' '.$style['submenu-posts-post_margin_left'].' !important;
				}
					.suppa_post div.suppa_post_link_container {
						background-color: '.$style['latest_posts_link_bg_color'].' !important ;
					}
					.suppa_post span {
						font-size:'.$style['submenu-posts-post_link_font_font_size'].'px !important;
						font-family:'.$style['submenu-posts-post_link_font_font_family'].' !important; 
						'.$style['submenu-posts-post_link_font_font_family_style'].'

						padding: '.$style['submenu-posts-post_link_padding_top'].' '.$style['submenu-posts-post_link_padding_right'].' '.$style['submenu-posts-post_link_padding_bottom'].' '.$style['submenu-posts-post_link_padding_left'].' !important;

						/* color/bg/border */
						color:'.$style['submenu-posts-post_link_font_font_color'].';	
					}
						.suppa_post span:hover ,
						.suppa_post span:hover {
							/* color/bg/border */
							color:'.$style['submenu-posts-post_link_color_hover'].' !important;	
						}

					.suppa_latest_posts_view_all{
						font-size:'.$style['submenu-posts-post_link_font_font_size'].'px !important;
						font-family:'.$style['submenu-posts-post_link_font_font_family'].' !important; 
						'.$style['submenu-posts-post_link_font_font_family_style'].'
						padding: '.$style['submenu-posts-post_margin_top'].' '.$style['submenu-posts-post_margin_left'].' ;
						margin: '.$style['submenu-posts-post_margin_top'].' '.$style['submenu-posts-post_margin_left'].';
 						
						/* color/bg/border */
						color:'.$style['latest_posts_view_all_color'].';
						background-color: '.$style['latest_posts_view_all_bg'].' !important ;
					}

					.suppa_latest_posts_view_all:hover{
						/* color/bg/border */
						color:'.$style['latest_posts_view_all_color_hover'].';
						background-color: '.$style['latest_posts_view_all_bg_hover'].' !important ;
					}



				/** ----------------------------------------------------------------
				 ******** SubMenu Mega Posts 
				 ---------------------------------------------------------------- **/
				.suppa_menu_mega_posts .suppa_mega_posts_categories{
					margin-top:'. $style['submenu-megaposts-post_margin_top'] .';
				}
				.suppa_menu_mega_posts .suppa_mega_posts_categories a {
					font-family : '. $style['submenu-megaposts-cat_link_font_font_family'] .';
					font-size:'.$style['submenu-megaposts-cat_link_font_font_size'].'px !important;
					'. $style['submenu-megaposts-cat_link_font_font_family_style'] .'
					color :'. $style['submenu-megaposts-cat_link_font_font_color'] .';
					padding-top:'. $style['submenu-megaposts-cat_link_padding_top'] .';
					padding-left:'. $style['submenu-megaposts-cat_link_padding_left'] .';
					padding-bottom:'. $style['submenu-megaposts-cat_link_padding_bottom'] .';
					padding-right:'. $style['submenu-megaposts-cat_link_padding_right'] .';
				
					background-color:'. $style['submenu-megaposts-cat_link_bg_color'] .';
				}
				.suppa_menu_mega_posts .suppa_mega_posts_categories a .suppa_item_title{
					font-family : '. $style['submenu-megaposts-cat_link_font_font_family'] .';
					font-size:'.$style['submenu-megaposts-cat_link_font_font_size'].'px !important;
					'. $style['submenu-megaposts-cat_link_font_font_family_style'] .'
					color :'. $style['submenu-megaposts-cat_link_font_font_color'] .';					
				}
				.suppa_menu_mega_posts .suppa_mega_posts_categories a:hover,
				.suppa_menu_mega_posts .suppa_mega_posts_categories a.suppa_mega_posts_cat_selected {
					background-color:'. $style['submenu-megaposts-cat_link_bg_color_hover'] .';
					color :'. $style['submenu-megaposts-cat_link_color_hover'] .';
				}

				.suppa_menu_mega_posts .suppa_mega_posts_categories a:hover .suppa_item_title,
				.suppa_menu_mega_posts .suppa_mega_posts_categories a.suppa_mega_posts_cat_selected .suppa_item_title{
					color :'. $style['submenu-megaposts-cat_link_color_hover'] .';
				}
				.suppa_mega_posts_post_article {
					margin-top:'. $style['submenu-megaposts-post_margin_top'] .';
					margin-left:'. $style['submenu-megaposts-post_margin_left'] .';
					margin-bottom:'. $style['submenu-megaposts-post_margin_bottom'] .';
					margin-right:'. $style['submenu-megaposts-post_margin_right'] .';				
				}

				.suppa_mega_posts_post_article span{
					font-family : '. $style['submenu-megaposts-post_link_font_font_family'] .';
					font-size:'.$style['submenu-megaposts-post_link_font_font_size'].' !important;
					'. $style['submenu-megaposts-post_link_font_font_family_style'] .'
					color :'. $style['submenu-megaposts-post_link_font_font_color'] .';
					
					padding-top:'. $style['submenu-megaposts-post_link_padding_top'] .';
					padding-left:'. $style['submenu-megaposts-post_link_padding_left'] .';
					padding-bottom:'. $style['submenu-megaposts-post_link_padding_bottom'] .';
					padding-right:'. $style['submenu-megaposts-post_link_padding_right'] .';
				
				}

				.suppa_mega_posts_post_article div.suppa_post_link_container {
					background-color: '.$style['mega_posts_link_bg_color'].' !important ;
				}

				.suppa_mega_posts_post_article:hover span{
					color :'. $style['submenu-megaposts-post_link_color_hover'] .';
				}

				.suppa_mega_posts_post_article {
					width : '. $style['submenu-megaposts-post_width'] .' !important;
				}
					.suppa_mega_posts_post_article img{
						width : '. $style['submenu-megaposts-post_width'] .' !important;
						height : '. $style['submenu-megaposts-post_height'] .' !important;
					}

				/** ----------------------------------------------------------------
				 ******** Submenu Mega Posts Arrow Style
				 ---------------------------------------------------------------- **/	
				.suppa_menu_mega_posts .suppa_mega_posts_categories a .suppa_mega_posts_arrow {
					top:'.$style['submenu-megaposts-cat_link_arrow_position_top'].' !important;
					right:'.$style['submenu-megaposts-cat_link_arrow_position_right'].' !important;
				}
				.suppa_menu_mega_posts .suppa_mega_posts_categories a .suppa_mega_posts_arrow span {
					font-size:'.$style['submenu-megaposts-cat_link_arrow_width'].' !important;
					/* color/bg/border */
					color:'.$style['submenu-megaposts-cat_link_arrow_color'].';
				}
				.suppa_menu_mega_posts .suppa_mega_posts_categories a:hover .suppa_mega_posts_arrow span {
					color:'.$style['submenu-megaposts-cat_link_arrow_color_hover'].' !important;
				}

				/** Needed for suppa_frontend.js **/
				.suppa_menu_mega_posts .suppa_mega_posts_cat_selected .suppa_mega_posts_arrow span{
					color:'.$style['submenu-megaposts-cat_link_arrow_color_hover'].' !important;
				}



				/** ----------------------------------------------------------------
				 ******** Submenu Mega Links Style
				 ---------------------------------------------------------------- **/	
				.suppa_column {
					margin-left:'.$style['submenu_column_right_margin'].' !important;
					margin-bottom:'.$style['submenu_column_right_margin'].' !important;
				}

				/* Column Title */
				.suppa_column_title {
					font-size:'.$style['submenu-links-title_link_font_font_size'].' !important;
					font-family:'.$style['submenu-links-title_link_font_font_family'].' !important;
					'.$style['submenu-links-title_link_font_font_family_style'].'
					color:'.$style['submenu-links-title_link_font_font_color'].';
					border-bottom-color:'.$style['submenu-links-title_bottom_border_color'].';
					padding: '.$style['submenu-links-title_padding_top'].' '.$style['submenu-links-title_padding_right'].' '.$style['submenu-links-title_padding_bottom'].' '.$style['submenu-links-title_padding_left'].';
				}
					.suppa_column_title .suppa_item_title {
						font-size:'.$style['submenu-links-title_link_font_font_size'].' !important;
						font-family:'.$style['submenu-links-title_link_font_font_family'].' !important;
						'.$style['submenu-links-title_link_font_font_family_style'].'
						color:'.$style['submenu-links-title_link_font_font_color'].';
					}

				.suppa_column_title:hover {
					padding-left : '.( (int)$style['submenu-links-title_padding_left'] + 10 ).'px !important;
					color:'.$style['submenu-links-title_link_color_hover'].' !important;
				}		
					.suppa_column_title:hover .suppa_item_title {
						color:'.$style['submenu-links-title_link_color_hover'].' !important;
					}

				/* Column Links */
				.suppa_column_link{
					font-size:'.$style['submenu-links-links_font_font_size'].'px !important;
					font-family:'.$style['submenu-links-links_font_font_family'].' !important;
					'.$style['submenu-links-links_font_font_family_style'].'
					color:'.$style['submenu-links-links_font_font_color'].';
					padding-right:'.$style['submenu-links-links_padding_right'].';
					padding-left: '.$style['submenu-links-links_padding_left'].' ;
					padding-top: '.$style['submenu-links-links_padding_top'].' ;
					padding-bottom: '.$style['submenu-links-links_padding_bottom'].' ;
				}
					.suppa_column_link .suppa_item_title {
						font-size:'.$style['submenu-links-links_font_font_size'].'px !important;
						font-family:'.$style['submenu-links-links_font_font_family'].' !important;
						'.$style['submenu-links-links_font_font_family_style'].'
						color:'.$style['submenu-links-links_font_font_color'].';
					}

				.suppa_column_link:nth-child(2) {
					margin-top:'.$style['submenu-links-links_padding_top'].' !important;
				}

				.suppa_column_link:hover {
					color:'.$style['submenu-links-links_color_hover'].' !important;
					padding-left : '.( (int)$style['submenu-links-links_padding_left'] + 10 ).'px !important;
				}
					.suppa_column_link:hover .suppa_item_title {
						color:'.$style['submenu-links-links_color_hover'].' !important;
					}

				/** ----------------------------------------------------------------
				 ******** Submenu Mega Links Title Icons Style
				 ---------------------------------------------------------------- **/	

				/* Title Icon */
				.suppa_column_title .ctf_suppa_fa_box {
					color:'.$style['submenu-links-title_link_font_font_color'].';
				}
				.suppa_column_title:hover .ctf_suppa_fa_box {
					color:'.$style['submenu-links-title_link_color_hover'].' !important;
				}

				/* Link Icon */
				.suppa_column_link .ctf_suppa_fa_box {
					color:'.$style['submenu-links-links_font_font_color'].';
				}
				.suppa_column_link:hover .ctf_suppa_fa_box {
					color:'.$style['submenu-links-links_color_hover'].' !important;
				}

				/** F.Awesome Icons **/
				.suppa_column_title .suppa_FA_icon {
					font-size:'.$style['submenu_mega_title_fontawesome_icons_size'].' !important;
					margin-top: '.$style['submenu_mega_title_fontawesome_icon_margin_top'].' !important;
					padding-right: '.$style['submenu_mega_title_fontawesome_icon_margin_right'].' !important;
				}

				/** Uploaded Icons **/
				.suppa_column_title .suppa_UP_icon {
					width : '.$style['submenu_mega_title_uploaded_icons_width'].' !important; 
					height : '.$style['submenu_mega_title_uploaded_icons_height'].' !important; 
					margin-top: '.$style['submenu_mega_title_normal_icon_margin_top'].' !important;
					padding-right: '.$style['submenu_mega_title_normal_icon_margin_right'].' !important;
				}

				/** ----------------------------------------------------------------
				 ******** Submenu Mega Links Links Icons Style
				 ---------------------------------------------------------------- **/	
				/** F.Awesome Icons **/
				.suppa_column_link .suppa_FA_icon {
					font-size:'.$style['submenu_mega_links_fontawesome_icons_size'].' !important;
					padding-top: '.$style['submenu_mega_links_fontawesome_icon_margin_top'].' !important;
					padding-right: '.$style['submenu_mega_links_fontawesome_icon_margin_right'].' !important;
				}

				/** Uploaded Icons **/
				.suppa_column_link .suppa_UP_icon {
					width : '.$style['submenu_mega_links_uploaded_icons_width'].' !important; 
					height : '.$style['submenu_mega_links_uploaded_icons_height'].' !important; 
					padding-top: '.$style['submenu_mega_links_normal_icon_margin_top'].' !important;
					padding-right: '.$style['submenu_mega_links_normal_icon_margin_right'].' !important;
				}



				/** ----------------------------------------------------------------
				 ******** Submenu DropDown Style
				 ---------------------------------------------------------------- **/	
				.suppa_menu_dropdown > .suppa_submenu a {
					font-size:'.$style['submenu-dropdown-link_font_font_size'].'px !important;
					font-family:'.$style['submenu-dropdown-link_font_font_family'].' !important;
					'.$style['submenu-dropdown-link_font_font_family_style'].'
					color:'.$style['submenu-dropdown-link_font_font_color'].';
					border-bottom:1px solid '.$style['submenu_dropdown_link_border_color'].';
					padding: '.$style['submenu_dropdown_link_padding_top'].' '.$style['submenu_dropdown_link_padding_right'].' '.$style['submenu_dropdown_link_padding_bottom'].' '.$style['submenu_dropdown_link_padding_left'].';
				}
					.suppa_menu_dropdown > .suppa_submenu a .suppa_item_title{
						font-size:'.$style['submenu-dropdown-link_font_font_size'].'px !important;
						font-family:'.$style['submenu-dropdown-link_font_font_family'].' !important;
						'.$style['submenu-dropdown-link_font_font_family_style'].'
						color:'.$style['submenu-dropdown-link_font_font_color'].';					
					}

				.suppa_menu_dropdown > .suppa_submenu div:hover > a ,
				.suppa_menu_dropdown > .suppa_submenu a:hover {
					color:'.$style['submenu-dropdown-link_color_hover'].';
					background-color:'.$style['submenu_dropdown_link_bg_hover'].';
					padding-left : '.( (int)$style['submenu_dropdown_link_padding_left'] + ( (int)$style['submenu_dropdown_link_padding_left'] / 3 ) ).'px;
				}
					.suppa_menu_dropdown > .suppa_submenu div:hover > a .suppa_item_title,
					.suppa_menu_dropdown > .suppa_submenu a:hover .suppa_item_title{
						color:'.$style['submenu-dropdown-link_color_hover'].';
					}

				/** Needed for suppa_frontend.js **/
				.suppa_menu_class_dropdown_levels_hover > a {
					color:'.$style['submenu-dropdown-link_color_hover'].' !important;
					background-color:'.$style['submenu_dropdown_link_bg_hover'].' !important;
				}
				.suppa_menu_class_dropdown_levels_hover > a .suppa_item_title{
					color:'.$style['submenu-dropdown-link_color_hover'].' !important;
				}

				/** ----------------------------------------------------------------
				 ******** Submenu DropDown Icons Style
				 ---------------------------------------------------------------- **/	
				.suppa_menu_dropdown > .suppa_submenu a .suppa_FA_icon{
					color:'.$style['submenu-dropdown-link_font_font_color'].';					

				}

				.suppa_menu_dropdown > .suppa_submenu div:hover > a .suppa_FA_icon,
				.suppa_menu_dropdown > .suppa_submenu a:hover .suppa_FA_icon{
					color:'.$style['submenu-dropdown-link_color_hover'].';
				}

				/** Needed for suppa_frontend.js **/
				.suppa_menu_class_dropdown_levels_hover > a .ctf_suppa_fa_box{
					color:'.$style['submenu-dropdown-link_color_hover'].' !important;
				}

				/** F.Awesome Icons **/
				.suppa_menu_dropdown .suppa_submenu .suppa_FA_icon {
					font-size:'.$style['submenu_dropdown_links_fontawesome_icons_size'].' !important;
					margin-top: '.$style['submenu_dropdown_links_fontawesome_icon_margin_top'].' !important;
					padding-right: '.$style['submenu_dropdown_links_fontawesome_icon_margin_right'].' !important;
				}

				/** Uploaded Icons **/
				.suppa_menu_dropdown .suppa_submenu .suppa_UP_icon {
					width : '.$style['submenu_dropdown_links_uploaded_icons_width'].' !important; 
					height : '.$style['submenu_dropdown_links_uploaded_icons_height'].' !important; 
					margin-top: '.$style['submenu_dropdown_links_normal_icon_margin_top'].' !important;
					padding-right: '.$style['submenu_dropdown_links_normal_icon_margin_right'].' !important;
				}


				/** ----------------------------------------------------------------
				 ******** Submenu DropDown Arrow Style
				 ---------------------------------------------------------------- **/	
				.suppa_menu_dropdown .suppa_submenu a .era_suppa_arrow_box {
					top:'.$style['dropdown-links-arrow_position_top'].' !important;
					right:'.$style['dropdown-links-arrow_position_right'].' !important;
				}
				.suppa_menu_dropdown .suppa_submenu a .era_suppa_arrow_box span {
					font-size:'.$style['dropdown-links-arrow_width'].' !important;
					/* color/bg/border */
					color:'.$style['dropdown-links-arrow_color'].';
				}
				.suppa_menu_dropdown div:hover > a .era_suppa_arrow_box span {
					color:'.$style['dropdown-links_arrow_color_hover'].' !important;
				}

				/** Needed for suppa_frontend.js **/
				.suppa_menu_class_dropdown_levels_hover > a .era_suppa_arrow_box span{
					color:'.$style['dropdown-links_arrow_color_hover'].' !important;
				}


				/** ----------------------------------------------------------------
				 ******** Search Form Style
				 ---------------------------------------------------------------- **/	
				.suppa_rwd_menu_search {
					background-color:'.$style['submenu-search-input_bg_color'].' !important;
				}
				.suppa_menu_search .suppa_search_form{
					margin-top:'.$style['submenu-search_margin_top'].' ;
					margin-left:'.$style['submenu-search_margin_left'].' ;
					margin-right:'.$style['submenu-search_margin_right'].' ;

				}

				.suppa_menu_search .suppa_search_form .suppa_search_input,
				.suppa_rwd_menu_search .suppa_search_form .suppa_search_input{
						width:'.$style['submenu-search-input_width'].';
						height:'.$style['submenu-search-input_height'].';
						line-height:'.$style['submenu-search-input_height'].' !important;

						font-size:'.$style['submenu-search-text_font_font_size'].'px;
						font-family:'.$style['submenu-search-text_font_font_family'].' !important;
						'.$style['submenu-search-text_font_font_family_style'].'

						padding-right:'.$style['submenu-search-text_padding_right'].' !important;
						padding-left:'.$style['submenu-search-text_padding_left'].' !important;

						/* color/bg/border */
						background-color:'.$style['submenu-search-input_bg_color'].' !important;
						color:'.$style['submenu-search-text_font_font_color'].' !important;

						-moz-border-radius: '.$style['submenu-search-input-borderradius_top_left'].' '.$style['submenu-search-input-borderradius_top_right'].' '.$style['submenu-search-input-borderradius_bottom_right'].' '.$style['submenu-search-input-borderradius_bottom_left'].';
						-webkit-border-radius: '.$style['submenu-search-input-borderradius_top_left'].' '.$style['submenu-search-input-borderradius_top_right'].' '.$style['submenu-search-input-borderradius_bottom_right'].' '.$style['submenu-search-input-borderradius_bottom_left'].';
						border-radius: '.$style['submenu-search-input-borderradius_top_left'].' '.$style['submenu-search-input-borderradius_top_right'].' '.$style['submenu-search-input-borderradius_bottom_right'].' '.$style['submenu-search-input-borderradius_bottom_left'].';
			
						/* Prevent background color leak outs */
						-webkit-background-clip: padding-box; 
						-moz-background-clip:    padding; 
						background-clip:         padding-box;
				
				}

				.suppa_search_input::-webkit-input-placeholder {
				   color:'.$style['submenu-search-text_font_font_color'].' !important;
				}

				.suppa_search_input:-moz-placeholder { /* Firefox 18- */
				   color:'.$style['submenu-search-text_font_font_color'].' !important; 
				}

				.suppa_search_input::-moz-placeholder {  /* Firefox 19+ */
				   color:'.$style['submenu-search-text_font_font_color'].' !important; 
				}

				.suppa_search_input:-ms-input-placeholder {  
				   color:'.$style['submenu-search-text_font_font_color'].' !important; 
				}

				.suppa_menu_search .suppa_search_form button ,
				.suppa_rwd_menu_search .suppa_search_form button{
					height:'.$style['submenu-search-input_height'].' !important;
					line-height:'.$style['submenu-search-input_height'].' !important;

					background-color:'.$style['submenu-search-button_bg_color'].';
					padding-right:'.$style['submenu-search-button_icon_padding_left_right'].';
					padding-left:'.$style['submenu-search-button_icon_padding_left_right'].';

					/* CSS3 Border Radius */
					-webkit-border-radius: 0px '.$style['submenu-search-input-borderradius_top_right'].' '.$style['submenu-search-input-borderradius_bottom_right'].' 0px; 
					-moz-border-radius: 0px '.$style['submenu-search-input-borderradius_top_right'].' '.$style['submenu-search-input-borderradius_bottom_right'].' 0px; 
					border-radius: 0px '.$style['submenu-search-input-borderradius_top_right'].' '.$style['submenu-search-input-borderradius_bottom_right'].' 0px; 

					/* Prevent background color leak outs */
					-webkit-background-clip: padding-box; 
					-moz-background-clip:    padding; 
					background-clip:         padding-box;
				
				}
				.suppa_menu_search .suppa_search_icon,
				.suppa_rwd_menu_search .suppa_search_icon{
					font-size : '.$style['submenu-search-button_icon_size'].';
					color: '.$style['submenu-search-button_icon_color'].';
				}

				.suppa_menu_search .suppa_search_form_big {
					z-index : '.( (int)$style['menu_z_index'] + 50 ).' !important; 
				}
				.suppa_menu_search .suppa_search_form_big,
				.suppa_menu_search .suppa_search_form_big input{
					width:'.$style['menu_width'].' !important;
				}

				.suppa_menu_search .suppa_search_form_big,
				.suppa_menu_search .suppa_search_form_big input{
					height:'.$style['menu_height'].' !important;
				}

				.suppa_menu_search .suppa_search_form_big input,
				.suppa_search_form_big .suppa_search_icon_remove{
					font-size:'.( (int)$style['submenu-search-text_font_font_size'] * 1.5 ).'px !important;
				}

				.suppa_search_form_big .suppa_search_icon_remove {
					color:'.$style['submenu-search-text_font_font_color'].' !important;
					height:'.$style['menu_height'].' !important;
					line-height:'.$style['menu_height'].' !important;
				}



				/** ----------------------------------------------------------------
				 ******** Social Links Style
				 ---------------------------------------------------------------- **/	
				.suppa_menu_social a.suppa_top_level_link{
				 	padding-left:'.$style['social-links-fontawesome_icon_margin_left'].' !important;
					padding-right:'.$style['social-links-fontawesome_icon_margin_right'].' !important;
				}
				
				/** ----------------------------------------------------------------
				 ******** Social Links Icons Style
				 ---------------------------------------------------------------- **/	
				/** F.Awesome Icons **/
				.suppa_menu_social .suppa_top_level_link .suppa_FA_icon_only {
					font-size:'.$style['social_links_fontawesome_icons_size'].' !important;
					padding-top: '.$style['social_links_fontawesome_icon_margin_top'].' !important;
					padding-right: '.$style['social_links_fontawesome_icon_margin_right'].' !important;
				}

				/** Uploaded Only Icons **/
				.suppa_menu_social .suppa_top_level_link .suppa_UP_icon_only {
					width : '.$style['social_links_uploaded_icons_width'].' !important; 
					height : '.$style['social_links_uploaded_icons_height'].' !important; 
					padding-top: '.$style['social_links_normal_icon_margin_top'].' !important;
					padding-right: '.$style['social_links_normal_icon_margin_right'].' !important;
				}




				/** ----------------------------------------------------------------
				 ******** Responsive Web Design Style
				 ---------------------------------------------------------------- **/	

				.suppa_rwd_top_button_container{
					height:'.$style['menu_height'].' !important;
					background-color:'.$style['menu_bg_bg_color'].';
					
					/* CSS3 Gradient */ 
					background-image: -webkit-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: -moz-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: -o-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: -ms-linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;
					background-image: linear-gradient(top, '.$style['menu_bg_gradient_from'].', '.$style['menu_bg_gradient_to'].') ;	
				
					'.$menu_background.'

					/* Borders */
					border-top: '.$style['menu_border_top_size'].' solid '.$style['menu_border_top_color'].';
					border-right: '.$style['menu_border_right_size'].' solid '.$style['menu_border_right_color'].';
					border-bottom: '.$style['menu_border_bottom_size'].' solid '.$style['menu_border_bottom_color'].';
					border-left: '.$style['menu_border_left_size'].' solid '.$style['menu_border_left_color'].';
				}

				.suppa_rwd_menus_container {
					/* color/bg/border */
					background-color:'.$style['submenu-bg_bg_color'].';
									
					/* CSS3 Gradient */ 
					background-image: -webkit-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: -moz-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: -o-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: -ms-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
				}


				/* Top Button & Text */
				.suppa_rwd_button,
				.suppa_rwd_text {
					line-height:'.$style['menu_height'].' !important;
				}
				.suppa_rwd_button {
					padding-right:'.$style['rwd_3bars_icon_right_margin'].' !important;
					padding-left:'.$style['rwd_3bars_icon_right_margin'].' !important;
					line-height:'.$style['menu_height'].' !important;
				}
				.suppa_rwd_button,
				.suppa_rwd_button span{
					font-size:'.$style['rwd_3bars_icon_size'].' !important;
					color:'.$style['rwd_3bars_icon_color'].' !important;
				}
				.suppa_rwd_text{
					font-size:'.$style['rwd_text_font_font_size'].'px !important;
					font-family:'.$style['rwd_text_font_font_family'].' !important;
					'.$style['rwd_text_font_font_family_style'].'
					color :'.$style['rwd_text_font_font_color'].' !important;
					
					padding: 0px '.$style['rwd_text_left_margin'].' !important;
					line-height:'.$style['menu_height'].' !important;
				}


				/* Submenu for latest posts, mega links, mega posts, html */
				.suppa_mega_posts_allposts_posts,
				.suppa_rwd_submenu_posts,
				.suppa_rwd_submenu_html,
				.suppa_rwd_submenu_columns_wrap {
					background-color:'.$style['submenu-bg_bg_color'].';
					border-bottom: '.$style['submenu-border-bottom_size'].' solid '.$style['submenu-border-bottom_color'].';

					/* CSS3 Gradient */ 
					background-image: -webkit-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: -moz-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: -o-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: -ms-linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;
					background-image: linear-gradient(top, '.$style['submenu-bg-gradient_from'].', '.$style['submenu-bg-gradient_to'].') ;

					'.$submenu_background_image.'

					/* Padding */
					padding-top:'.$style['rwd_submenu_padding_top'].' !important;
					padding-bottom:'.$style['rwd_submenu_padding_bottom'].' !important;
					padding-right:'.$style['rwd_submenu_padding_right'].' !important;
					padding-left:'.$style['rwd_submenu_padding_left'].' !important;

					/* CSS3 Box Shadow */
					-moz-box-shadow   : 0px 0px '.$style['submenu-boxshadow_blur'].' '.$style['submenu-boxshadow_distance'].' '.$style['submenu-boxshadow_color'].';
					-webkit-box-shadow: 0px 0px '.$style['submenu-boxshadow_blur'].' '.$style['submenu-boxshadow_distance'].' '.$style['submenu-boxshadow_color'].';
					box-shadow        : 0px 0px '.$style['submenu-boxshadow_blur'].' '.$style['submenu-boxshadow_distance'].' '.$style['submenu-boxshadow_color'].';
				}


				/* Main Links */
				.suppa_rwd_menu > a,
				.suppa_rwd_submenu_mega_posts > a,
				.suppa_rwd_submenu > .suppa_dropdown_item_container > a {
					font-size:'.$style['rwd_main_links_font_font_size'].'px !important;
					font-family:'.$style['rwd_main_links_font_font_family'].' !important;
					'.$style['rwd_main_links_font_font_family_style'].'
					color :'.$style['rwd_main_links_font_font_color'].';

					height:'.$style['rwd_main_links_height'].' !important;
					line-height:'.$style['rwd_main_links_height'].' !important;
					border-bottom:1px solid '.$style['rwd_main_links_bottom_border_color'].' !important;
					background-color:'.$style['rwd_main_links_bg'].';
				}
				.suppa_rwd_menu > a .suppa_item_title,
				.suppa_rwd_submenu_mega_posts > a .suppa_item_title,
				.suppa_rwd_submenu > .suppa_dropdown_item_container > a .suppa_item_title{
					font-size:'.$style['rwd_main_links_font_font_size'].'px !important;
					font-family:'.$style['rwd_main_links_font_font_family'].' !important;
					'.$style['rwd_main_links_font_font_family_style'].'
					color :'.$style['rwd_main_links_font_font_color'].';
				}

				.era_rwd_suppa_link_both_open,
				.suppa_rwd_menu:hover > a,
				.suppa_rwd_submenu_mega_posts > a:hover,
				.suppa_rwd_menu .suppa_dropdown_item_container a:hover {
					color :'.$style['rwd-main_links_color_hover'].' !important;
					background-color :'.$style['rwd_main_links_bg_hover'].' !important;
				}
				.era_rwd_suppa_link_both_open .suppa_item_title,
				.suppa_rwd_menu:hover > a .suppa_item_title,
				.suppa_rwd_submenu_mega_posts > a:hover .suppa_item_title,
				.suppa_rwd_menu .suppa_dropdown_item_container a:hover .suppa_item_title{
					color :'.$style['rwd-main_links_color_hover'].' !important;			
				}
				.suppa_rwd_menus_container{
					border-top:1px solid '.$style['rwd_main_links_bottom_border_color'].' !important;
				}

				.suppa_rwd_menu > a{
					padding-left:'.$style['rwd_main_links_left_margin'].' !important;
				}
				.suppa_rwd_menu_dropdown > .suppa_rwd_submenu > .suppa_dropdown_item_container > a {
					padding-left:'.( (int)$style['rwd_main_links_left_margin'] * 1.8 ).'px !important;
				}
				.suppa_rwd_menu_dropdown > .suppa_rwd_submenu > .suppa_dropdown_item_container > .suppa_rwd_submenu > .suppa_dropdown_item_container > a {
					padding-left:'.( (int)$style['rwd_main_links_left_margin'] * 2.8 ).'px !important;
				}
				.suppa_rwd_menu_dropdown > .suppa_rwd_submenu > .suppa_dropdown_item_container > .suppa_rwd_submenu > .suppa_dropdown_item_container > .suppa_rwd_submenu > .suppa_dropdown_item_container > a {
					padding-left:'.( (int)$style['rwd_main_links_left_margin'] * 3.8 ).'px !important;
				}

				/* Main Links Arrows */
				.suppa_rwd_menu > a .era_rwd_suppa_arrow_box {
					font-size:'.$style['rwd_main_links_arrow_width'].' !important;
						
					/* color/bg/border */
					color:'.$style['rwd_main_links_arrow_color'].';
				}
				.suppa_rwd_menu:hover > a .era_rwd_suppa_arrow_box {
					color:'.$style['rwd_main_links_arrow_color_hover'].' !important;
				}
				.era_rwd_suppa_arrow_both_open{
					color :'.$style['rwd_main_links_font_font_color'].' !important;
					background-color:'.$style['rwd_main_links_bg'].' !important;
				}
				.era_rwd_suppa_link_both_open{
					color :'.$style['rwd-main_links_color_hover'].' !important;
					background-color :'.$style['rwd_main_links_bg_hover'].' !important;
				}


				/* Logo */
				.suppa_rwd_logo{
					height:'.$style['menu_height'].';
				}
				.suppa_rwd_logo img{
					padding : '.$style['rwd_logo_padding_top'].' '.$style['rwd_logo_padding_right'].' 0px '.$style['rwd_logo_padding_left'].' !important;
				}


				/* Search Form */
				.suppa_rwd_menu_search{
					border-bottom:1px solid '.$style['rwd_main_links_bottom_border_color'].' !important;
				}
				.suppa_rwd_menu_search form {
					padding-top:'.$style['rwd_submenu_padding_top'].' !important;
					padding-bottom:'.$style['rwd_submenu_padding_bottom'].' !important;
					padding-right:'.$style['rwd_submenu_padding_right'].' !important;
					padding-left:'.$style['rwd_submenu_padding_left'].' !important;
				}	
				.suppa_rwd_menu_search form input[type="text"]{
					color:'.$style['submenu-search-text_font_font_color'].' !important;
				}


				/* Mega Posts */
				.suppa_rwd_submenu_mega_posts > a {
					padding-left:'.( (int)$style['rwd_main_links_left_margin'] * 2 ).'px !important;
				}


				/** ----------------------------------------------------------------
				 ******** RWD Icons Style
				 ---------------------------------------------------------------- **/	
				.suppa_rwd_menu > a .ctf_suppa_fa_box,
				.suppa_rwd_menu .suppa_dropdown_item_container .ctf_suppa_fa_box{
					color :'.$style['rwd_main_links_font_font_color'].';

				}
				.era_rwd_suppa_link_both_open .ctf_suppa_fa_box,
				.suppa_rwd_menu:hover > a .ctf_suppa_fa_box,
				.suppa_rwd_menu .suppa_dropdown_item_container a:hover .ctf_suppa_fa_box {
					color :'.$style['rwd-main_links_color_hover'].' !important;
				}


				/** F.Awesome Icons **/
				.suppa_rwd_menu > a .suppa_FA_icon,
				.suppa_rwd_menu .suppa_dropdown_item_container .suppa_FA_icon
				 {
					font-size:'.$style['rwd_links_fontawesome_icons_size'].' !important;
					padding-top: '.$style['rwd_links_fontawesome_icon_margin_top'].' !important;
					padding-right: '.$style['rwd_links_fontawesome_icon_margin_right'].' !important;
				}

				/** Uploaded Icons **/
				.suppa_rwd_menu > a .suppa_UP_icon,
				.suppa_rwd_menu .suppa_dropdown_item_container a .suppa_UP_icon
				 {
					width : '.$style['rwd_links_uploaded_icons_width'].' !important; 
					height : '.$style['rwd_links_uploaded_icons_height'].' !important; 
					padding-top: '.$style['rwd_links_normal_icon_margin_top'].' !important;
					padding-right: '.$style['rwd_links_normal_icon_margin_right'].' !important;
				}

			';

			$custom_css .= $style['custom-css'];

			/** Add Generated CSS To File **/
			$my_file = $upload_dir['basedir'].'/suppamenu/suppa_custom_style.css';
			$handle = @fopen($my_file, 'w');
			fwrite($handle, $custom_css);
			fclose($handle);

			/** Save Generated File Link To Database **/
			update_option('suppa_custom_style_file' , $upload_dir['baseurl'].'/suppamenu/suppa_custom_style.css' );










			/** Save JS Settings To Files & DB **/
			// Save JS Settings
			// Responsive Design Enable or Not
			$responsive_enable = 'off';
			if( $style['settings-responsive_enable'] == 'on' )
			{
				$responsive_enable = 'on';
			}
			// jQuery Mode 
			$jquery_mode = 'off';
			if( $style['settings-jquery_enable'] == 'on' )
			{
				$jquery_mode = 'on';
			}

			/** Suppa JS Parameters **/
			$suppa_settings = "
			suppa_js_settings = new Object();

			suppa_js_settings.jquery_mode 		= '".$style['settings-jquery_enable']."';
			suppa_js_settings.jquery_trig 		= '".$style['settings-jquery_trigger']."';
			suppa_js_settings.jquery_anim 		= '".$style['settings-jquery_animation']."';
			suppa_js_settings.jquery_easings	= '".$style['settings-jquery_easings']."';
			suppa_js_settings.jquery_time 		= ".(int)$style['settings-jquery_animation_time'].";
			suppa_js_settings.rwd_enable 		= '".$style['settings-responsive_enable']."';
			suppa_js_settings.rwd_enable_desk	= '".$style['settings-responsive_enable_desktops']."';

			suppa_js_settings.rwd_start_width	= ".(int)$style['settings_responsive_start_width'].";
			suppa_js_settings.rwd_text			= '".$style['settings-responsive_text']."';

			suppa_js_settings.box_layout		= '".$style['menu-layout']."';
			suppa_js_settings.scroll_enable		= '".$style['settings-sticky_enable']."';
			suppa_js_settings.scroll_enable_mob	= '".$style['settings-sticky_mobile_enable']."';

			suppa_js_settings.modern_search 	= '".$style['settings_modern_search']."';
			suppa_js_settings.rwd_search 		= '".$style['settings_rwd_search_form_display']."';

			suppa_js_settings.logo_enable 		= '".$style['logo_enable']."';
			suppa_js_settings.rwd_logo_enable 	= '".$style['rwd_logo_enable']."';

			";

			/** Save Generated Settings To File **/
			$my_file 	= $upload_dir['basedir'].'/suppamenu/suppa_js_settings.js';
			$handle 	= @fopen($my_file, 'w');
			fwrite($handle, $suppa_settings);
			fclose($handle);

			/** Save Generated File Link To Database **/
			update_option('suppa_js_settings_file' , $upload_dir['baseurl'].'/suppamenu/suppa_js_settings.js' );





		}

	}
}// End IF