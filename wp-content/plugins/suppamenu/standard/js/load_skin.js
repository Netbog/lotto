/**
 * Admin Script
 *
 * @package 	CTFramework
 * @author		Sabri Taieb ( codezag )
 * @copyright	Copyright (c) Sabri Taieb
 * @link		http://codetemp.com
 * @since		Version 1.0
 *
 */

jQuery(document).ready(function(){

	jQuery('.suppa_skins_list li').click(function(){
		var $this = jQuery(this);
		
		if( !$this.is('.suppa_skin_selected') )
		{
			$this.siblings().removeClass('suppa_skin_selected');
			$this.addClass('suppa_skin_selected');
			jQuery('.skin_holder').val( $this.attr('data-skin_file') );
		}
	});

	jQuery(document).on('click','#suppa_load_skin',function(event){
		event.preventDefault();

			var $form 			= jQuery(this).parents('#codetemp_form');
			var $plugin_url 	= $form.find('#codetemp_plugin_url').val();

			// Set Loading Image 
			var $html_ajax_img_load = $plugin_url + 'core/img/ajax-loader.gif'; 
			var $html_ajax_img_save = $plugin_url + 'core/img/success_icon.png';
			var $html_ajax_res = jQuery('.codetemp_ajax_response');

			$html_ajax_res.children('img').attr( 'src' , $html_ajax_img_load ); 
			$html_ajax_res.children('span').text('');
			$html_ajax_res.show();


		var skin_url = jQuery('.skin_url').val() + jQuery('.skin_holder').val();

		jQuery.getJSON( skin_url, function( data ) {
			jQuery.each( data, function( key, val ) {
				$op = jQuery('.codetemp_pages_container').find('*[name="'+key+'"]');
				$op.val( val );

				if( $op.is('.ctf_option_colorpicker') )
					$op.next('.ctf_option_colorpicker_color').css({'background-color':val})	;
			});

			$html_ajax_res.children('img').attr( 'src' , $html_ajax_img_save ); 
			$html_ajax_res.children('span').html('<div>All skin options are loaded to the admin area option</div><br/><br/><div>please save settings</div>');
		});
	});

	jQuery('.ctf_option_colorpicker').change(function(){
		jQuery(this).next('.ctf_option_colorpicker_color').css({'background-color': jQuery(this).val() });
	});
});